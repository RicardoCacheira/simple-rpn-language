if (process.platform == "win32") {
    const fs = require("fs");
    const path = require("path");
    const exec = require("child_process").execSync
    console.log("Reading package.json")
    var json = JSON.parse(fs.readFileSync("package.json"));
    console.log("Backing up package.json")
    fs.renameSync("package.json", "package.json.bak");
    console.log("Generating modified package.json and .cmd files")
    for (var b in json.bin) {
        var binPath = path.resolve(json.bin[b])
        json.bin[b] = path.join(path.dirname(binPath), path.basename(binPath).split(".")[0] + ".cmd");
        fs.writeFileSync(json.bin[b], "node " + binPath + " %*")
    }
    fs.writeFileSync("./package.json", JSON.stringify(json))
    console.log("Running npm link")
    exec("npm link")
    console.log("Removing modified package.json")
    fs.unlinkSync("package.json")
    console.log("Reloading package.json backup")
    fs.renameSync("package.json.bak", "package.json");
} else {
    const exec = require("child_process").execSync
    console.log("Running npm link")
    exec("npm link")
}