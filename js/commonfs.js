const fs = require("fs");
const exec = require("child_process").execSync;

function isSep(char) {
    return char == "/" || char == "\\";
}

if (process.platform != "win32") {
    class CommomFS {
        constructor(path = ".") {
            this.currentPath = process.cwd();
            this.setCWD(path);
        }
        _isExistingDir(path) {
            return fs.existsSync(path) && fs.lstatSync(path).isDirectory();
        }
        normalize(path) {
            if (this.isValid(path)) {
                var isAbsolute = this.isAbsolute(path);
                var aux = [];
                var res = [];
                var buffer = ""
                var c;
                for (var i = 0; i < path.length; i++) {
                    c = path.charAt(i);
                    if (isSep(c)) {
                        aux.push(buffer);
                        buffer = "";
                    } else {
                        buffer += c;
                    }
                }
                aux.push(buffer);
                for (var part of aux) {
                    if (part == "..") {
                        if (res.length == 0) {
                            if (!isAbsolute) {
                                res.push("..");
                            }
                        } else {
                            if (res[res.length - 1] == "..") {
                                res.push("..");
                            } else {
                                res.pop();
                            }
                        }
                    } else if (!(part == "." || part == "")) {
                        res.push(part);
                    }
                }
                if (isAbsolute) {
                    return "/" + res.join("/");
                } else {
                    return res.join("/");
                }
            } else {
                throw "Invalid Path";
            }
        }
        resolve(path) {
            if (this.isAbsolute(path)) {
                return this.normalize(path);
            } else {
                return this.normalize(this.currentPath + "/" + path);
            }

        }
        isAbsolute(path) {
            return this.isValid(path) && (isSep(path.charAt(0)));
        }
        isRelative(path) {
            return this.isValid(path) && !this.isAbsolute(path)
        }
        isValid(path) {
            return path.length > 0;
        }
        /*
        name(path) {
            var last = this.split(path).pop();
            if (last.charAt())
                return this.split(path).pop().split(".").pop();
        }
        dir(path) {
    
        }*/
        getCWD() {
            return this.currentPath;
        }
        setCWD(path) {
            path = this.resolve(path);
            if (this._isExistingDir(path)) {
                this.currentPath = path;
            } else {
                throw "Invalid directory"
            }
        }
        isFile(path) {
            try {
                path = this.resolve(path);
                return fs.existsSync(path) && fs.lstatSync(path).isFile();
            } catch (e) {
                return false;
            }
        }
        isDir(path) {
            try {
                path = this.resolve(path);
                return fs.existsSync(path) && fs.lstatSync(path).isDirectory();
            } catch (e) {
                return false;
            }
        }
        readFile(path) {
            path = this.resolve(path);
            return fs.readFileSync(path)
        }
        writeFile(text, path) {
            path = this.resolve(path);
            fs.writeFileSync(path, text);
        }
        removeFile(path) {
            path = this.resolve(path);
            fs.unlinkSync(path);
        }
        listDir(path) {
            path = this.resolve(path);
            return fs.readdirSync(path);
        }
        makeDir(path) {
            path = this.resolve(path);
            fs.mkdirSync(path);
        }
        removeDir(path) {
            path = this.resolve(path);
            fs.rmdirSync(path);
        }
        newFS() {
            return new CommomFS(this.currentPath)
        }
    }
    module.exports = CommomFS
} else {
    class CommomFSWin {
        constructor(path = ".") {
            this.currentPath = this._toUnixPath(process.cwd());
            this.setCWD(path);
        }
        _toUnixPath(path) {		
            path = path.replace(/\//g,"\\").split(":");
            var drive = path[0].charAt(0).toLowerCase();
            path = path[1].split("\\");
            path.shift();
            return "/" + drive + "/" + path.join("/");
        }
        _toWinPath(path) {
            if (path == "/") {
                return "";
            }
            if (path.charAt(1) == ":") {
                return path;
            }
            path = this.resolve(path);

            path = path.split("/");
            path.shift();
            path[0] = path[0].toUpperCase() + ":";
            return path.join("\\");
        }
        _isExistingDir(path) {
            return fs.existsSync(path) && fs.lstatSync(path).isDirectory();
        }

        _normalize(path) {
            var isAbsolute = isSep(path.charAt(0));
            var aux = [];
            var res = [];
            var buffer = ""
            var c;
            for (var i = 0; i < path.length; i++) {
                c = path.charAt(i);
                if (isSep(c)) {
                    aux.push(buffer);
                    buffer = "";
                } else {
                    buffer += c;
                }
            }
            aux.push(buffer);
            for (var part of aux) {
                if (part == "..") {
                    if (res.length == 0) {
                        if (!isAbsolute) {
                            res.push("..");
                        }
                    } else {
                        if (res[res.length - 1] == "..") {
                            res.push("..");
                        } else {
                            res.pop();
                        }
                    }
                } else if (!(part == "." || part == "")) {
                    res.push(part);
                }
            }
            if (isAbsolute) {
                if (res.length > 0) {
                    if (res[0].length > 0) {
                        res[0] = res[0].toLowerCase();
                    }
                    return "/" + res.join("/");
                } else {
                    return "/"
                }
            } else {
                return res.join("/");
            }
        }

        normalize(path) {
            path = this._normalize(path)
            if (this._isValid(path)) {
                return path;
            } else {
                throw "Invalid Path";
            }
        }
        resolve(path) {
            if (path.length > 1 && path.charAt(1) == ":") {
                return this.normalize(this._toUnixPath(path));
            } else if (isSep(path.charAt(0))) {
                return this.normalize(path);
            } else {
                return this.normalize(this.currentPath + "/" + path);
            }
        }
        isAbsolute(path) {
            return this.isValid(path) && isSep(path.charAt(0));
        }
        isRelative(path) {
            return this.isValid(path) && !isSep(path.charAt(0));
        }
        _isValid(path) {
            path = path.trim();
            if (path.length > 0) {
                var c;
                for (var i = 0; i < path.length; i++) {
                    c = path.charAt(i);
                    if (c == ":") {
                        return false;
                    }
                }
                if (isSep(path.charAt(0))) {
                    if (path.length == 2) {
                        return (path.charAt(1) >= "A" && path.charAt(1) <= "Z") || (path.charAt(1) >= "a" && path.charAt(1) <= "z");
                    } else if (path.length > 2) {
                        return isSep(path.charAt(2)) && ((path.charAt(1) >= "A" && path.charAt(1) <= "Z") || (path.charAt(1) >= "a" && path.charAt(1) <= "z"));
                    }
                    return true;
                }
                return true;
            }
            return false;
        }
        isValid(path) {
            return this._isValid(this._normalize(path));
        }
        /*
        name(path) {
            var last = this.split(path).pop();
            if (last.charAt())
                return this.split(path).pop().split(".").pop();
        }
        dir(path) {
    
        }*/
        getCWD() {
            return this.currentPath;
        }
        setCWD(path) {
            path = this._toWinPath(path);
            if (this._isExistingDir(path)) {
                this.currentPath = this._toUnixPath(path);
            } else if (path == "") {
                this.currentPath = "/";
            } else {
                throw "Invalid directory"
            }
        }
        exists(path) {
            return fs.existsSync(this._toWinPath(path))
        }
        isFile(path) {
            try {
                path = this._toWinPath(path);
                return fs.existsSync(path) && fs.lstatSync(path).isFile();
            }
            catch (e) {
                return false;
            }
        }
        isDir(path) {
            try {
                path = this._toWinPath(path);
                return fs.existsSync(path) && fs.lstatSync(path).isDirectory();
            } catch (e) {
                return false;
            }
        }
        readFile(path) {
            path = this._toWinPath(path);
            return fs.readFileSync(path)
        }
        writeFile(text, path) {
            path = this._toWinPath(path);
            fs.writeFileSync(path, text);
        }
        removeFile(path) {
            path = this._toWinPath(path);
            fs.unlinkSync(path);
        }
        listDir(path) {
            path = this._toWinPath(path);
            if (path) {
                return fs.readdirSync(path);
            } else {
                return exec("wmic logicaldisk get name").toString().split("\r\r\n").filter((val, i) => { return i > 0 && val.trim().length > 0 }).map((val) => { return "/" + val.charAt(0).toLowerCase() });
            }
        }
        makeDir(path) {
            path = this._toWinPath(path);
            fs.mkdirSync(path);
        }
        removeDir(path) {
            path = this._toWinPath(path);
            fs.rmdirSync(path);
        }
        newFS() {
            return new CommomFSWin(this._toWinPath(this.currentPath))
        }
    }
    module.exports = CommomFSWin
}
