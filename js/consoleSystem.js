const InputCatcher = require("./inputCatcher");

const inputCatcher = new InputCatcher()

module.exports = {
    print: function (log) {
        process.stdout.write(log);
    },
    warning: function (err) {
        process.stdout.write("\n" + String.fromCharCode(27) + "[33m" + err + String.fromCharCode(27) + "[0m\n");
    },
    error: function (err) {
        process.stdout.write("\n" + String.fromCharCode(27) + "[31m" + err + String.fromCharCode(27) + "[0m\n");
    },
    getPriorityKey: function (callback) {
        inputCatcher.addPriorityInput(callback);
    },
    getKey: function (callback) {
        inputCatcher.addInput(callback);
    },
    removeGetKey: function (callback) {
        inputCatcher.removeInput(callback);
    },
    clear: function () {
        process.stdout.write(String.fromCharCode(27) + "[1;1H" + String.fromCharCode(27) + "[J");
    },
    dimensions: function () {
        return [process.stdout.columns, process.stdout.rows]
    }
}