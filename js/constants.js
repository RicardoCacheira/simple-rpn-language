module.exports.TOKEN_TRUE = "true";
module.exports.TOKEN_FALSE = "false";
module.exports.TOKEN_EXIT = "exit";
module.exports.TOKEN_LIST_START = "[";
module.exports.TOKEN_LIST_END = "]";
module.exports.TOKEN_COMMAND_START = "(";
module.exports.TOKEN_COMMAND_END = ")";
module.exports.TOKEN_BLOCK_START = "{";
module.exports.TOKEN_BLOCK_END = "}";
module.exports.TOKEN_SET_VAR = "$";
module.exports.TOKEN_GET_VAR = "@";
module.exports.TOKEN_TOP_VAR = "^";
module.exports.TOKEN_LOCAL_VAR = "~";
module.exports.TOKEN_STRING_CHAR = '"';
module.exports.TOKEN_COMMENT_CHAR = '#';
module.exports.TOKEN_COMMENT_BLOCK_CHAR = '-';