const keyList = require("./inputKey").list;
function createHexString() {
    var buffer = Buffer.allocUnsafe(arguments.length);
    for (var i = 0; i < arguments.length; i++) {
        buffer.writeUInt8(arguments[i], i);
    }
    return buffer.toString();
}

const KEY_UP = createHexString(0x1b, 0x5b, 0x41);
const KEY_DOWN = createHexString(0x1b, 0x5b, 0x42);
const KEY_RIGHT = createHexString(0x1b, 0x5b, 0x43);
const KEY_LEFT = createHexString(0x1b, 0x5b, 0x44);
const SHIFT_KEY_UP = createHexString(0x1b, 0x5b, 0x31, 0x3b, 0x32, 0x41);
const SHIFT_KEY_DOWN = createHexString(0x1b, 0x5b, 0x31, 0x3b, 0x32, 0x42);
const SHIFT_KEY_RIGHT = createHexString(0x1b, 0x5b, 0x31, 0x3b, 0x32, 0x43);
const SHIFT_KEY_LEFT = createHexString(0x1b, 0x5b, 0x31, 0x3b, 0x32, 0x44);
const KEY_HOME_UNIX = createHexString(0x1b, 0x5b, 0x48);
const KEY_END_UNIX = createHexString(0x1b, 0x5b, 0x46);
const KEY_HOME_WIN = createHexString(0x1b, 0x5b, 0x31, 0x7e);
const KEY_END_WIN = createHexString(0x1b, 0x5b, 0x34, 0x7e);
const KEY_PG_UP = createHexString(0x1b, 0x5b, 0x35, 0x7e);
const KEY_PG_DOWN = createHexString(0x1b, 0x5b, 0x36, 0x7e);
const KEY_RETURN = createHexString(0x0d);
const KEY_BS_UNIX = createHexString(0x7f);
const KEY_BS_WIN = createHexString(0x08);
const KEY_DEL = createHexString(0x1b, 0x5b, 0x33, 0x7e);
const KEY_ESC = createHexString(0x1b);
const KEY_TAB = createHexString(0x09);
const KEY_F1_UNIX = createHexString(0x1b, 0x4f, 0x50);
const KEY_F1_WIN = createHexString(0x1b, 0x5b, 0x5b, 0x41);
const KEY_F2_UNIX = createHexString(0x1b, 0x4f, 0x51);
const KEY_F2_WIN = createHexString(0x1b, 0x5b, 0x5b, 0x42);
const KEY_F3_UNIX = createHexString(0x1b, 0x4f, 0x52);
const KEY_F3_WIN = createHexString(0x1b, 0x5b, 0x5b, 0x43);
const KEY_F4_UNIX = createHexString(0x1b, 0x4f, 0x53);
const KEY_F4_WIN = createHexString(0x1b, 0x5b, 0x5b, 0x44);
const KEY_F5_UNIX = createHexString(0x1b, 0x5b, 0x31, 0x35, 0x7e);
const KEY_F5_WIN = createHexString(0x1b, 0x5b, 0x5b, 0x45);
const KEY_F6 = createHexString(0x1b, 0x5b, 0x31, 0x37, 0x7e);
const KEY_F7 = createHexString(0x1b, 0x5b, 0x31, 0x38, 0x7e);
const KEY_F8 = createHexString(0x1b, 0x5b, 0x31, 0x39, 0x7e);
const KEY_F9 = createHexString(0x1b, 0x5b, 0x32, 0x30, 0x7e);
const KEY_F10 = createHexString(0x1b, 0x5b, 0x32, 0x31, 0x7e);
const KEY_F11 = createHexString(0x1b, 0x5b, 0x32, 0x33, 0x7e);
const KEY_F12 = createHexString(0x1b, 0x5b, 0x32, 0x34, 0x7e);
const CTRL_Q = createHexString(0x11);
const CTRL_W = createHexString(0x17);
const CTRL_E = createHexString(0x05);
const CTRL_R = createHexString(0x12);
const CTRL_T = createHexString(0x14);
const CTRL_Y = createHexString(0x19);
const CTRL_U = createHexString(0x15);
//CTRL_I its the same as Tab
const CTRL_O = createHexString(0x0f);
const CTRL_P = createHexString(0x10);
const CTRL_A = createHexString(0x01);
const CTRL_S = createHexString(0x13);
const CTRL_D = createHexString(0x04);
const CTRL_F = createHexString(0x06);
const CTRL_G = createHexString(0x07);
//CTRL_H its the same as Backspace
const CTRL_J = createHexString(0x0a);
const CTRL_K = createHexString(0x0b);
const CTRL_L = createHexString(0x0c);
const CTRL_Z = createHexString(0x1a);
const CTRL_X = createHexString(0x18);
const CTRL_C = createHexString(0x03);
const CTRL_V = createHexString(0x16);
const CTRL_B = createHexString(0x02);
const CTRL_N = createHexString(0x0e);
//CTRL_M its the same as Return

class InputCatcher {
    constructor() {
        this.inputList = [];
        process.stdin.setRawMode(true);
        process.stdin.setEncoding('utf8');
        var ic = this
        process.stdin.on("data", function (data) {
            switch (data) {
                case KEY_UP:
                    ic.send(keyList.KEY_UP)
                    break;
                case KEY_DOWN:
                    ic.send(keyList.KEY_DOWN)
                    break;
                case KEY_RIGHT:
                    ic.send(keyList.KEY_RIGHT)
                    break;
                case KEY_LEFT:
                    ic.send(keyList.KEY_LEFT)
                    break;
                case SHIFT_KEY_UP:
                    ic.send(keyList.SHIFT_KEY_UP)
                    break;
                case SHIFT_KEY_DOWN:
                    ic.send(keyList.SHIFT_KEY_DOWN)
                    break;
                case SHIFT_KEY_RIGHT:
                    ic.send(keyList.SHIFT_KEY_RIGHT)
                    break;
                case SHIFT_KEY_LEFT:
                    ic.send(keyList.SHIFT_KEY_LEFT)
                    break;
                case KEY_HOME_UNIX:
                case KEY_HOME_WIN:
                    ic.send(keyList.KEY_HOME)
                    break;
                case KEY_END_UNIX:
                case KEY_END_WIN:
                    ic.send(keyList.KEY_END)
                    break;
                case KEY_PG_UP:
                    ic.send(keyList.KEY_PG_UP)
                    break;
                case KEY_PG_DOWN:
                    ic.send(keyList.KEY_PG_DOWN)
                    break;
                case KEY_RETURN:
                    ic.send(keyList.KEY_RETURN)
                    break;
                case KEY_BS_UNIX:
                case KEY_BS_WIN:
                    ic.send(keyList.KEY_BS)
                    break;
                case KEY_DEL:
                    ic.send(keyList.KEY_DEL)
                    break;
                case KEY_ESC:
                    ic.send(keyList.KEY_ESC)
                    break;
                case KEY_TAB:
                    ic.send(keyList.KEY_TAB)
                    break;
                case KEY_F1_UNIX:
                case KEY_F1_WIN:
                    ic.send(keyList.KEY_F1)
                    break;
                case KEY_F2_UNIX:
                case KEY_F2_WIN:
                    ic.send(keyList.KEY_F2)
                    break;
                case KEY_F3_UNIX:
                case KEY_F3_WIN:
                    ic.send(keyList.KEY_F3)
                    break;
                case KEY_F4_UNIX:
                case KEY_F4_WIN:
                    ic.send(keyList.KEY_F4)
                    break;
                case KEY_F5_UNIX:
                case KEY_F5_WIN:
                    ic.send(keyList.KEY_F5)
                    break;
                case KEY_F6:
                    ic.send(keyList.KEY_F6)
                    break;
                case KEY_F7:
                    ic.send(keyList.KEY_F7)
                    break;
                case KEY_F8:
                    ic.send(keyList.KEY_F8)
                    break;
                case KEY_F9:
                    ic.send(keyList.KEY_F9)
                    break;
                case KEY_F10:
                    ic.send(keyList.KEY_F10)
                    break;
                case KEY_F11:
                    ic.send(keyList.KEY_F11)
                    break;
                case KEY_F12:
                    ic.send(keyList.KEY_F12)
                    break;
                case CTRL_Q:
                    ic.send(keyList.CTRL_Q);
                    break;
                case CTRL_W:
                    ic.send(keyList.CTRL_W);
                    break;
                case CTRL_E:
                    ic.send(keyList.CTRL_E);
                    break;
                case CTRL_R:
                    ic.send(keyList.CTRL_R);
                    break;
                case CTRL_T:
                    ic.send(keyList.CTRL_T);
                    break;
                case CTRL_Y:
                    ic.send(keyList.CTRL_Y);
                    break;
                case CTRL_U:
                    ic.send(keyList.CTRL_U);
                    break;
                case CTRL_O:
                    ic.send(keyList.CTRL_O);
                    break;
                case CTRL_P:
                    ic.send(keyList.CTRL_P);
                    break;
                case CTRL_A:
                    ic.send(keyList.CTRL_A);
                    break;
                case CTRL_S:
                    ic.send(keyList.CTRL_S);
                    break;
                case CTRL_D:
                    ic.send(keyList.CTRL_D);
                    break;
                case CTRL_F:
                    ic.send(keyList.CTRL_F);
                    break;
                case CTRL_G:
                    ic.send(keyList.CTRL_G);
                    break;
                case CTRL_J:
                    ic.send(keyList.CTRL_J);
                    break;
                case CTRL_K:
                    ic.send(keyList.CTRL_K);
                    break;
                case CTRL_L:
                    ic.send(keyList.CTRL_L);
                    break;
                case CTRL_Z:
                    ic.send(keyList.CTRL_Z);
                    break;
                case CTRL_X:
                    ic.send(keyList.CTRL_X);
                    break;
                case CTRL_C:
                    //TODO only exit if no input exists
                    process.exit()
                    break;
                case CTRL_V:
                    ic.send(keyList.CTRL_V);
                    break;
                case CTRL_B:
                    ic.send(keyList.CTRL_B);
                    break;
                case CTRL_N:
                    ic.send(keyList.CTRL_N);
                    break;
                default:
                    ic.send(data);
            }
        })
    }
    addPriorityInput(input) {
        this.inputList.unshift(input);
    }
    addInput(input) {
        this.inputList.push(input);
    }
    removeInput(input) {
        var pos = this.inputList.indexOf(input);
        if (pos != -1) {
            this.inputList.splice(pos, 1);
        }
    }
    send(data) {
        if (this.inputList.length) {
            this.inputList.shift()(data);
        }
    }
}

module.exports = InputCatcher;