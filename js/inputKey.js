var currId = 0;

class InputKey {
    constructor(str) {
        this.id = currId;
        this.str = "Key: " + str
        currId++;
    }
    toString() {
        return this.str;
    }
    equals(o) {
        if (o instanceof InputKey) {
            return this.id == o.id;
        }
        return false;
    }
}

function registerKey(str) {
    module.exports.list[str] = new InputKey(str);
}

module.exports.class = InputKey;
module.exports.list = {};

registerKey("KEY_UP");
registerKey("KEY_DOWN");
registerKey("KEY_RIGHT");
registerKey("KEY_LEFT");
registerKey("SHIFT_KEY_UP");
registerKey("SHIFT_KEY_DOWN");
registerKey("SHIFT_KEY_RIGHT");
registerKey("SHIFT_KEY_LEFT");
registerKey("KEY_HOME");
registerKey("KEY_END");
registerKey("KEY_PG_UP");
registerKey("KEY_PG_DOWN");
registerKey("KEY_RETURN");
registerKey("KEY_BS");
registerKey("KEY_DEL");
registerKey("KEY_ESC");
registerKey("KEY_TAB");
registerKey("KEY_F1");
registerKey("KEY_F2");
registerKey("KEY_F3");
registerKey("KEY_F4");
registerKey("KEY_F5");
registerKey("KEY_F6");
registerKey("KEY_F7");
registerKey("KEY_F8");
registerKey("KEY_F9");
registerKey("KEY_F10");
registerKey("KEY_F11");
registerKey("KEY_F12");
registerKey("CTRL_Q");
registerKey("CTRL_W");
registerKey("CTRL_E");
registerKey("CTRL_R");
registerKey("CTRL_T");
registerKey("CTRL_Y");
registerKey("CTRL_U");
registerKey("CTRL_O");
registerKey("CTRL_P");
registerKey("CTRL_A");
registerKey("CTRL_S");
registerKey("CTRL_D");
registerKey("CTRL_F");
registerKey("CTRL_G");
registerKey("CTRL_J");
registerKey("CTRL_K");
registerKey("CTRL_L");
registerKey("CTRL_Z");
registerKey("CTRL_X");
registerKey("CTRL_C");
registerKey("CTRL_V");
registerKey("CTRL_B");