const constants = require("./constants")
const SRLTypes = require('./srlTypes');
const nativeTypes = SRLTypes.nativeTypeList;
const SRLParser = require("./parser");
const tokenType = require("./tokenType");
const SRLCommand = require("./srlCommand");
const SRLFunction = require("./srlFunction");
const SRLNativeFunction = require("./srlNativeFunction");
const SRLMultiFunction = require("./srlMultiFunction");
const ik = require("./inputKey");
const InputKey = ik.class;
const keyList = ik.list;
const utils = require("./utils")
const CustomType = SRLTypes.CustomType;
const CommonFS = require("./commonfs");

const SRLIModes = {
    HOLD: 0,
    NORMAL: 1
}

class SRLI {
    constructor(parser, baseVars, cwd, consoleSystem, guiSystem, exitFunc, showErrors) {
        this.parser = parser;
        this.mode = SRLIModes.HOLD;
        this.stack = [];
        this.buffer = [];
        this.lorBuffer = [];
        this.vars = [baseVars];
        this.lists = [];
        this.commandTokens = [];
        this.currentToken = undefined;
        this.currentTokenValue = undefined;
        this.commands = 0;
        this.commandLevel = 0;
        this.types = new Map();
        this.typeNames = [];
        this.error = "";
        this.showErrors = showErrors
        this.localVars = new Map();
        this.fileSystem = new CommonFS(cwd);
        this.consoleSystem = consoleSystem;
        this.guiSystem = guiSystem;
        this.exitFunc = exitFunc;
        this.asyncRequests = 0;
        this.asyncResults = [];
        this.baseImage = utils.copyMap(baseVars);
        this.VMList = new Map();
        //load native types
        for (var type of Object.keys(nativeTypes)) {
            type = nativeTypes[type];
            this.vars[0].set(type.name, type);
        }
        //load keys
        for (var key of Object.keys(keyList)) {
            this.vars[0].set(key, keyList[key]);
        }
    }
    exit() {
        this.buffer = [];
        this.mode = SRLIModes.HOLD;
        this.exitFunc();
    }
    areValidParams(ops) {
        if (this.stack.length < ops.length) {
            return false;
        }
        for (var i = 0; i < ops.length; i++) {
            if (ops[i] !== nativeTypes.ANY && ops[i] !== this.typeOf(this.stack[this.stack.length - ops.length + i])) {
                return false;
            }
        }
        return true;
    }

    getParams(num) {
        return this.stack.splice(this.stack.length - num, num);
    }

    push(val) {
        this.stack.push(val);
    }

    pop() {
        if (this.stack.length) {
            return this.stack.pop();
        }
        this.throwCurrentTokenError("Poping an empty stack.");
    }

    getVar() {
        for (var i = this.commandLevel; i >= 0; i--) {
            if (this.vars[i].has(this.currentTokenValue)) {
                return this.vars[i].get(this.currentTokenValue);
            }
        }
        return undefined;
    }

    isVar() {
        return this.getVar() != undefined;
    }

    setVar() {
        this.vars[this.commandLevel].set(this.currentTokenValue, this.pop());
    }

    loadVar() {
        var v = this.getVar();
        if (v instanceof SRLCommand) {
            this.processCommand(v);
        } else if (v instanceof SRLFunction || v instanceof SRLNativeFunction || v instanceof SRLMultiFunction) {
            v.checkAndRun(this);
        } else {
            this.push(v);
        }
    }

    pushVar() {
        this.push(this.getVar());
    }

    _getTopVarLevel() {
        for (var i = this.commandLevel - 1; i >= 0; i--) {
            if (this.vars[i].has(this.currentTokenValue)) {
                return i;
            }
        }
        return -1;
    }

    getTopVar() {
        for (var i = this.commandLevel - 1; i >= 0; i--) {
            if (this.vars[i].has(this.currentTokenValue)) {
                return this.vars[i].get(this.currentTokenValue);
            }
        }
        return undefined;
    }

    isTopVar() {
        return this.getTopVar() != undefined;
    }

    setTopVar() {
        this.vars[this._getTopVarLevel()].set(this.currentTokenValue, this.pop());
    }

    loadTopVar() {
        var v = this.getTopVar();
        if (v instanceof SRLCommand) {
            this.processCommand(v);
        } else if (v instanceof SRLFunction || v instanceof SRLNativeFunction || v instanceof SRLMultiFunction) {
            v.checkAndRun(this);
        } else {
            this.push(v);
        }
    }

    pushTopVar() {
        this.push(this.getTopVar());
    }

    getLocalVar() {
        return this.localVars.get(this.currentTokenValue);
    }

    isLocalVar() {
        return this.getLocalVar() != undefined;
    }

    setLocalVar() {
        this.localVars.set(this.currentTokenValue, this.pop());
    }

    loadLocalVar() {
        var v = this.getLocalVar();
        if (v instanceof SRLCommand) {
            this.processCommand(v);
        } else if (v instanceof SRLFunction || v instanceof SRLNativeFunction || v instanceof SRLMultiFunction) {
            v.checkAndRun(this);
        } else {
            this.push(v);
        }
    }

    pushLocalVar() {
        this.push(this.getLocalVar());
    }

    _setCurrentToken(token) {
        this.currentToken = token;
        this.currentTokenValue = token.value;
    }

    _startList() {
        this.lists.push(this.stackSize());
    }

    _endList() {
        var pos = this.lists.pop();
        if (pos > this.stackSize()) {
            this.throwCurrentTokenError("Stack size smaller in end. Stack size in the start: " + pos + ", Stack size in the end: " + this.stackSize());
        } else {
            this.push(this.stack.splice(pos, this.stackSize() - pos));
        }
    }

    dumpStack() {
        var stack = this.stack;
        this.stack = [];
        return stack;
    }

    stackSize() {
        return this.stack.length;
    }

    generateStackString() {
        var res = "Stack:\n\n"
        if (this.stackSize()) {
            for (var i = this.stack.length - 1; i >= 0; i--) {
                if (typeof this.stack[i] == "string") {
                    res += i + ": " + utils.formattedString(this.stack[i]) + "\n";
                } else {
                    res += i + ": " + this.valueToString(this.stack[i]) + "\n";
                }
            }
        } else {
            res += "None\n"
        }
        return res;
    }

    processCommand(com, callback, errorCallback) {
        this.buffer.unshift(...com.tokens, this._createCommandExecutionToken(callback, errorCallback));
    }

    _createCommandExecutionToken(callback, catchCallback) {
        this.commandLevel++;
        this.vars.push(new Map());
        return { token: tokenType.COMMAND_EXECUTION, callback: callback, catchCallback: catchCallback, line: this.currentToken.line };
    }

    _runCatchCallback(error) {
        var token
        while (this.buffer.length) {
            token = this.buffer.shift();
            if (token.token == tokenType.COMMAND_EXECUTION) {
                this.commandLevel--;
                this.vars.pop();
                if (token.catchCallback) {
                    token.catchCallback(error);
                    return;
                }
            }
        }
    }

    _pushStringCommand() {
        this._generateStringCommand(this.currentTokenValue);
    }

    _generateStringCommand(list, pos = 0, buffer = "") {
        var aux;
        var x = this;
        while (pos < list.length) {
            aux = list[pos];
            pos++;
            if (aux instanceof Array) {
                this.processCommand(new SRLCommand(aux), function () {
                    buffer += x.valueToString(x.pop());
                    x._generateStringCommand(list, pos, buffer);
                });
                return
            }
            buffer += aux;
        }
        this.push(buffer)
    }

    _process() {
        if (this.mode == SRLIModes.HOLD) {
            return false;
        }
        if (this.buffer.length) {
            this._setCurrentToken(this.buffer.shift());
            if (this.mode == SRLIModes.NORMAL) {
                switch (this.currentToken.token) {
                    case tokenType.VAR:
                        if (this.isVar()) {
                            this.loadVar();
                        } else {
                            this.throwCurrentTokenError("Undefined Variable '" + this.currentTokenValue + "'");
                        }
                        break;
                    case tokenType.TOP_VAR:
                        if (this.commandLevel) {
                            if (this.isTopVar()) {
                                this.loadTopVar();
                            } else {
                                this.throwCurrentTokenError("Undefined Top Variable '" + this.currentTokenValue + "'");
                            }
                        } else {
                            this.throwCurrentTokenError("Top Variable call on level 0");
                        }
                        break;
                    case tokenType.LOCAL_VAR:
                        if (this.isLocalVar()) {
                            this.loadLocalVar();
                        } else {
                            this.throwCurrentTokenError("Undefined Local Variable '" + this.currentToken.name + "'");
                        }
                        break;
                    case tokenType.GET_VAR:
                        if (this.isVar()) {
                            this.pushVar();
                        } else {
                            this.throwCurrentTokenError("Undefined Variable '" + this.currentTokenValue + "'");
                        }
                        break;
                    case tokenType.GET_TOP_VAR:
                        if (this.commandLevel) {
                            if (this.isTopVar()) {
                                this.pushTopVar();
                            } else {
                                this.throwCurrentTokenError("Undefined Top Variable '" + this.currentTokenValue + "'");
                            }
                        } else {
                            this.throwCurrentTokenError("Top Variable call on level 0");
                        }
                        break;
                    case tokenType.GET_LOCAL_VAR:
                        if (this.isLocalVar()) {
                            this.pushLocalVar();
                        } else {
                            this.throwCurrentTokenError("Undefined Variable '" + this.currentToken.name + "'");
                        }
                        break;
                    case tokenType.SET_VAR:
                        this.setVar();
                        break;
                    case tokenType.SET_TOP_VAR:
                        if (this.isTopVar()) {
                            this.setTopVar();
                        } else {
                            this.throwCurrentTokenError("Top Variable '" + this.currentTokenValue + "' does not exist");
                        }
                        break;
                    case tokenType.SET_LOCAL_VAR:
                        this.setLocalVar();
                        break;
                    case tokenType.NUMBER:
                    case tokenType.STRING:
                    case tokenType.BOOLEAN:
                    case tokenType.COMMAND:
                        this.push(this.currentTokenValue);
                        break;
                    case tokenType.STRING_COMMAND:
                        this._pushStringCommand();
                    case tokenType.LIST_START:
                        this._startList();
                        break;
                    case tokenType.LIST_END:
                        this._endList();
                        break;
                    case tokenType.COMMAND_EXECUTION:
                        this.commandLevel--;
                        this.vars.pop();
                        if (this.currentToken.callback) {
                            this.currentToken.callback();
                        }
                        if (this.commandLevel == 0) {
                            this.buffer.unshift(...this.lorBuffer);
                            this.lorBuffer = [];
                        }
                        break;
                    case tokenType.EXIT:
                        this.exit();
                        break;
                    case tokenType.HOLD:
                        break;
                    case tokenType.BLOCK_START:
                        break;
                    case tokenType.BLOCK_END:
                        break;
                    default:
                        throw "Invalid token type: " + this.currentToken.token
                }
            }
            return true;
        } else {
            if (this.asyncResults.length) {
                var result = this.asyncResults.shift();
                result[0]();
                this.buffer.push(...result[1].tokens);
                return true;
            } else if (this.asyncRequests == 0) {
                this.exit();
            } else {
                this.mode = SRLIModes.HOLD;
            }
        }
        return false;
    }

    throwCurrentTokenError(error) {
        this.throwError("Error on " + this.currentToken.codeName + ":" + this.currentToken.line + ":" + this.currentTokenValue + ": " + error + "\n");
    }

    throwError(error) {
        this._runCatchCallback(error);
        if (this.buffer.length == 0) {
            if (this.rootErrorCommand) {
                this.stack.push(error);
                this.buffer.push(...this.rootErrorCommand.tokens);
                this.rootErrorCommand = undefined;
                this.run();
            } else {
                if (this.showErrors) {
                    this.consoleSystem.error(error);
                }
                this.error = error;
            }
        }
    }

    addRootErrorCommand(com) {
        this.rootErrorCommand = com;
    }

    parse(code) {
        return this.parser.parse(code);
    }

    load(code) {
        this.buffer.push(...this.parse(code));
    }

    loadOnRoot(com) {
        if (this.commandLevel) {
            this.lorBuffer.push(...com.tokens);
        } else {
            this.buffer.unshift(...com.tokens);
        }
    }

    loadFile(path) {
        this.buffer.push(...this.parse(constants.TOKEN_COMMENT_CHAR + constants.TOKEN_COMMENT_BLOCK_CHAR + path + constants.TOKEN_COMMENT_BLOCK_CHAR + constants.TOKEN_COMMENT_CHAR + this.fileSystem.readFile(path)));
    }

    getCWD() {
        return this.fileSystem.getCWD();
    }

    setCWD(dir) {
        this.fileSystem.setCWD(dir);
    }

    resolvePath(path) {
       return this.fileSystem.resolve(path);
    }

    getVarList(prefix = "") {
        var res = [];
        for (var level of this.vars) {
            for (var v of level.keys()) {
                v = prefix + v;
                if (res.indexOf(v) == -1) {
                    res.push(v);
                }
            }
        }
        return res;
    }

    rotl() {
        this.stack.push(this.stack.shift());
    }

    rotr() {
        this.stack.unshift(this.stack.pop());
    }

    registerType(typeName, checkCom, toStringCom) {
        if (this.typeNames.includes(typeName)) {
            this.throwCurrentTokenError("Type \"" + typeName + "\" already exists.");
        } else {
            this.vars[0].set(typeName, new CustomType(typeName, checkCom, toStringCom));
            this.typeNames.push(typeName);
        }
    }

    pushCustomTypeValue(type, value) {
        type.createInstance(this, value);
    }

    typeOf(o) {
        var type = typeof o;
        if (type == "object") {
            if (o instanceof Array) {
                return nativeTypes.LIST;
            } else if (o instanceof SRLCommand) {
                return nativeTypes.COMMAND;
            } else if (o instanceof SRLFunction) {
                return nativeTypes.FUNCTION;
            } else if (o instanceof SRLNativeFunction) {
                return nativeTypes.NATIVE_FUNCTION;
            } else if (o instanceof SRLMultiFunction) {
                return nativeTypes.MULTI_FUNCTION;
            } else if (o instanceof InputKey) {
                return nativeTypes.KEY;
            } else if (o instanceof SRLTypes.SRLType) {
                return nativeTypes.TYPE;
            } else {
                return o.type;
            }
        } else if (type == "number") {
            return nativeTypes.NUMBER;
        } else if (type == "string") {
            return nativeTypes.STRING;
        } else if (type == "boolean") {
            return nativeTypes.BOOLEAN;
        }
    }

    isValidType(type) {
        return type instanceof SRLTypes.SRLType;
    }

    _listToString(list) {
        var res = "[ "
        if (list.length) {
            if (typeof list[0] == "string") {
                res += utils.formattedString(list[0]);
            } else {
                res += this.valueToString(list[0]);
            }
            for (var i = 1; i < list.length; i++) {
                if (typeof list[i] == "string") {
                    res += ", " + utils.formattedString(list[i]);
                } else {
                    res += ", " + this.valueToString(list[i]);
                }
            }
            res += " ]";
        } else {
            res += "]";
        }
        return res;
    }

    valueToString(value) {
        if (value instanceof Array) {
            return this._listToString(value);
        }
        return value.toString();
    }

    run() {
        if (this.mode == SRLIModes.HOLD) {
            this.mode = SRLIModes.NORMAL
            while (this._process()) { }
        }
    }

    setVMBaseImage() {
        this.baseImage = utils.copyMap(this.vars[0]);
    }

    _createVM() {
        var vm;
        const parent = this;
        var exitFunc = function () {
            parent.run();
        }
        vm = new SRLI(this.parser, utils.copyMap(this.baseImage), this.fileSystem, this.consoleSystem, this.guiSystem, exitFunc, false);
        return vm;
    }

    newVM() {
        var key = utils.generateRandomString(16);
        this.VMList.set(key, this._createVM());
        return key;
    }

    runVM(vmKey, command) {
        var vm = this.VMList.get(vmKey);
        if (vm) {
            this.hold();
            vm.loadOnRoot(command);
            vm.run();
        } else {
            this.throwCurrentTokenError("Invalid VM key.");
        }
    }

    dumpVMStack(vmKey) {
        var vm = this.VMList.get(vmKey)
        if (vm) {
            return vm.dumpStack();
        } else {
            this.throwCurrentTokenError("Invalid VM key.");
        }
    }

    setVMStack(vmKey, stack) {
        var vm = this.VMList.get(vmKey);
        if (vm) {
            vm.stack = stack;
        } else {
            this.throwCurrentTokenError("Invalid VM key.");
        }
    }

    getVMError(vmKey) {
        var vm = this.VMList.get(vmKey)
        if (vm) {
            var error = vm.error;
            vm.error = "";
            return error;
        } else {
            this.throwCurrentTokenError("Invalid VM key.");
        }
    }

    hold() {
        this.mode = SRLIModes.HOLD;
        //This token is inserted into the buffer so that if the command that called the hold function,
        //in case it was from the last token in the buffer, desn't have the hold accidentaly disabled by any async results on the system.
        if (this.buffer.length == 0) {
            this.buffer.push({ token: tokenType.HOLD });
        }
    }

    addAsyncRequest() {
        this.asyncRequests++;
    }

    loadAsyncResult(callback, com) {
        this.asyncRequests--;
        this.asyncResults.push([callback, com]);
        if (this.buffer.length == 0) {
            this.run();
        }
    }
}

module.exports.SRLIModes = SRLIModes
module.exports.createInterpreter = function (baseVars = [], consoleSystem, guiSystem, exitFunc) {
    const parser = new SRLParser();
    return new SRLI(parser, new Map(baseVars), ".", consoleSystem, guiSystem, exitFunc, true)
}