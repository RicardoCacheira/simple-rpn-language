const SRLTypes = require("./srlTypes")
const nativeTypes = SRLTypes.nativeTypeList;
const SRLCommand = require("./srlCommand")
const SRLFunction = require("./srlFunction")
const SRLNativeFunction = require("./srlNativeFunction")
const SRLMultiFunction = require("./srlMultiFunction");
const utils = require("./utils")
const numberCheck = require("./numberCheck");
const tokenType = require("./tokenType");
const tokenStrings = require("./tokenStrings");
const page = require("fs").readFileSync(__dirname + "/../wsCanvas.html");
const http = require('http');
const ws = require("nodejs-websocket");
var guiServerStarted = false
const CustomType = SRLTypes.CustomType;
const CustomTypeInstance = SRLTypes.CustomTypeInstance;

const BOOLEAN_OP = [nativeTypes.BOOLEAN];

module.exports = [
    ["+", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.NUMBER, nativeTypes.NUMBER],
            function (sys, n1, n2) {
                sys.push(n1 + n2);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.LIST, nativeTypes.LIST],
            function (sys, l1, l2) {
                sys.push(l1.concat(l2));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.COMMAND, nativeTypes.COMMAND],
            function (sys, com1, com2) {
                sys.push(com1.concat(com2));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.ANY],
            function (sys, str, x) {
                sys.push(str + sys.valueToString(x));
            }
        )
    ])],

    ["-", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.NUMBER, nativeTypes.NUMBER],
            function (sys, n1, n2) {
                sys.push(n1 - n2);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.LIST, nativeTypes.LIST],
            function (sys, l1, l2) {
                var res = [];
                for (var x of l1) {
                    if (!utils.contains(l2, x)) {
                        res.push(x);
                    }
                }
                sys.push(res);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.STRING],
            function (sys, s1, s2) {
                sys.push(utils.stringReplace(s1, s2, ""));
            }
        )
    ])],
    ["*", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, n1, n2) {
            sys.push(n1 * n2);
        }
    )],
    ["/", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, n1, n2) {
            sys.push(n1 / n2);
        }
    )],
    ["%", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, n1, n2) {
            sys.push(n1 % n2);
        }
    )],
    ["repeat", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.NUMBER],
        function (sys, com, num, curr = 0) {
            var x = this;
            if (num < 0) {
                sys.throwCurrentTokenError("Number of repeats must be positive");
            }
            if (curr < num) {
                sys.processCommand(com, function () { x.nativeFunc(sys, com, num, curr + 1) });
            }
        }
    )],
    ["for", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.COMMAND, nativeTypes.NUMBER],
            function (sys, com, num, curr = 0) {
                var x = this;
                if (num < 0) {
                    sys.throwCurrentTokenError("Number of repeats must be positive");
                }
                if (curr < num) {
                    sys.push(curr);
                    sys.processCommand(com, function () { x.nativeFunc(sys, com, num, curr + 1) });
                }
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.COMMAND, nativeTypes.LIST],
            function (sys, com, list, curr = 0) {
                var x = this;
                if (curr < list.length) {
                    sys.push(list[curr]);
                    sys.processCommand(com, function () { x.nativeFunc(sys, com, list, curr + 1) });
                }
            }
        )
    ])],
    ["while", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.COMMAND],
        function (sys, check, com) {
            var x = this;
            sys.processCommand(check, function () {
                if (sys.stackSize() > 0) {
                    var res = sys.pop();
                    if (typeof res == "boolean") {
                        if (res) {
                            sys.processCommand(com, function () { x.nativeFunc(sys, check, com) });
                        }
                    } else {
                        sys.throwCurrentTokenError("Non boolean value on top of the stack after check");
                    }
                } else {
                    sys.throwCurrentTokenError("Empty Stack after check");
                }
            });
        }
    )],
    ["filter", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.LIST],
        function (sys, com, list, res = [], curr = 0) {
            var x = this;
            if (curr < list.length) {
                sys.push(list[curr])
                sys.processCommand(com, function () {
                    if (sys.stackSize() > 0) {
                        var check = sys.pop();
                        if (typeof check == "boolean") {
                            if (check) {
                                res.push(list[curr]);
                            }
                            x.nativeFunc(sys, com, list, res, curr + 1);
                        } else {
                            sys.throwCurrentTokenError("Non boolean value on top of the stack after filtercheck");
                        }
                    } else {
                        sys.throwCurrentTokenError("Empty Stack after filter check");
                    }
                });
            } else {
                sys.push(res);
            }
        }
    )],
    ["map", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.LIST],
        function (sys, com, list, res = [], curr = 0) {
            var x = this;
            if (curr < list.length) {
                sys.push(list[curr])
                sys.processCommand(com, function () {
                    if (sys.stackSize() > 0) {
                        res.push(sys.pop());
                        x.nativeFunc(sys, com, list, res, curr + 1)
                    } else {
                        sys.throwCurrentTokenError("Empty Stack after map command");
                    }
                });
            } else {
                sys.push(res);
            }
        }
    )],
    ["reduce", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.LIST],
        function (sys, com, list, curr = 0) {
            var x = this;
            if (curr < list.length) {
                sys.push(list[curr]);
                if (curr > 0) {
                    sys.processCommand(com, function () {
                        x.nativeFunc(sys, com, list, curr + 1);
                    });
                } else {
                    x.nativeFunc(sys, com, list, 1);
                }
            }
        }
    )],
    ["if", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.COMMAND],
        function (sys, check, com) {
            sys.processCommand(check, function () {
                if (sys.stackSize() > 0) {
                    var res = sys.pop();
                    if (typeof res == "boolean") {
                        if (res) {
                            sys.processCommand(com);
                        }
                    } else {
                        sys.throwCurrentTokenError("Non boolean value on top of the stack after check");
                    }
                } else {
                    sys.throwCurrentTokenError("Empty Stack after check");
                }
            });
        }
    )],
    ["ifelse", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.COMMAND, nativeTypes.COMMAND],
        function (sys, check, com, comElse) {
            var x = this;
            sys.processCommand(check, function () {
                if (sys.stackSize() > 0) {
                    var res = sys.pop();
                    if (typeof res == "boolean") {
                        if (res) {
                            sys.processCommand(com);
                        } else {
                            sys.processCommand(comElse);
                        }
                    } else {
                        sys.throwCurrentTokenError("Non boolean value on top of the stack after check");
                    }
                } else {
                    sys.throwCurrentTokenError("Empty Stack after check");
                }
            });
        }
    )],
    ["switch", new SRLNativeFunction(
        [nativeTypes.LIST, nativeTypes.ANY],
        function (sys, list, a, curr = 0) {
            var x = this;
            if (curr == 0 && (list.length % 2) != 0) {
                sys.throwCurrentTokenError("Odd number of commands on the list");
            }
            if (curr < list.length) {
                var caseCom = list[curr];
                var ifEqualCom = list[curr + 1];
                sys.processCommand(caseCom, function () {
                    var aux = sys.pop();
                    if (aux != undefined) {
                        if (utils.equals(aux, a)) {
                            sys.processCommand(ifEqualCom);
                        } else {
                            x.nativeFunc(sys, list, a, curr + 2)
                        }
                    }
                });
            }
        }
    )],
    ["try", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.COMMAND],
        function (sys, tryCom, catchCom) {
            sys.processCommand(tryCom, undefined, function (error) {
                sys.push(error);
                sys.processCommand(catchCom);
            });
        }
    )],
    ["==", new SRLNativeFunction(
        [nativeTypes.ANY, nativeTypes.ANY],
        function (sys, a1, a2) {
            sys.push(utils.equals(a1, a2));
        }
    )],
    ["!=", new SRLNativeFunction(
        [nativeTypes.ANY, nativeTypes.ANY],
        function (sys, a1, a2) {
            sys.push(!utils.equals(a1, a2));
        }
    )],
    [">", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.NUMBER, nativeTypes.NUMBER],
            function (sys, n1, n2) {
                sys.push(n1 > n2);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.STRING],
            function (sys, s1, s2) {
                sys.push(s1 > s2);
            }
        )
    ])],
    ["<", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.NUMBER, nativeTypes.NUMBER],
            function (sys, n1, n2) {
                sys.push(n1 < n2);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.STRING],
            function (sys, s1, s2) {
                sys.push(s1 < s2);
            }
        )
    ])],
    [">=", new SRLMultiFunction([
        new SRLNativeFunction([nativeTypes.NUMBER, nativeTypes.NUMBER],
            function (sys, n1, n2) {
                sys.push(n1 >= n2);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.STRING],
            function (sys, s1, s2) {
                sys.push(s1 >= s2);
            }
        )
    ])],
    ["<=", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.NUMBER, nativeTypes.NUMBER],
            function (sys, n1, n2) {
                sys.push(n1 <= n2);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.STRING],
            function (sys, s1, s2) {
                sys.push(s1 <= s2);
            }
        )
    ])],
    ["&&", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.BOOLEAN, nativeTypes.BOOLEAN],
            function (sys, b1, b2) {
                sys.push(b1 && b2);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.COMMAND, nativeTypes.COMMAND],
            function (sys, c1, c2) {
                sys.processCommand(c1, function () {
                    var res = sys.pop()
                    if (typeof res == "boolean") {
                        if (res) {
                            sys.processCommand(c2, function () {
                                if (!sys.areValidParams(BOOLEAN_OP)) {
                                    sys.throwCurrentTokenError("Non boolean value on top of the stack after second command.");
                                }
                            });
                        } else {
                            sys.push(false);
                        }
                    } else {
                        sys.throwCurrentTokenError("Non boolean value on top of the stack after first command.");
                    }
                });
            }
        )
    ])],
    ["||", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.BOOLEAN, nativeTypes.BOOLEAN],
            function (sys, b1, b2) {
                sys.push(b1 || b2);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.COMMAND, nativeTypes.COMMAND],
            function (sys, c1, c2) {
                sys.processCommand(c1, function () {
                    var res = sys.pop()
                    if (typeof res == "boolean") {
                        if (res) {
                            sys.push(true);
                        } else {
                            sys.processCommand(c2, function () {
                                if (!sys.areValidParams(BOOLEAN_OP)) {
                                    sys.throwCurrentTokenError("Non boolean value on top of the stack after second command.");
                                }
                            });
                        }
                    } else {
                        sys.throwCurrentTokenError("Non boolean value on top of the stack after first command.");
                    }
                });
            }
        )
    ])],
    ["!", new SRLNativeFunction(
        [nativeTypes.BOOLEAN],
        function (sys, bool) {
            sys.push(!bool);
        }
    )],
    ["size", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.LIST],
            function (sys, list) {
                sys.push(list.length);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING],
            function (sys, str) {
                sys.push(str.length);
            }
        )
    ])],
    ["type", new SRLNativeFunction(
        [nativeTypes.ANY],
        function (sys, a) {
            sys.push(sys.typeOf(a));
        }
    )],
    ["toString", new SRLNativeFunction(
        [nativeTypes.ANY],
        function (sys, a) {
            sys.push(sys.valueToString(a));
        }
    )],
    ["toInt", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.NUMBER],
            function (sys, n) {
                sys.push(Number.parseInt(n));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING],
            function (sys, str) {
                if (numberCheck.isInt(str)) {
                    sys.push(Number.parseInt(str));
                } else {
                    sys.throwCurrentTokenError("Invalid Integer string");
                }
            }
        )
    ])],
    ["toNumber", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            if (numberCheck.isNumber(str)) {
                sys.push(Number.parseFloat(str));
            } else {
                sys.throwCurrentTokenError("Invalid Number string");
            }
        }
    )],
    ["isInt", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.NUMBER],
            function (sys, n) {
                sys.push(numberCheck.isInt(n.toString()));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING],
            function (sys, str) {
                sys.push(numberCheck.isInt(str));
            }
        )
    ])],
    ["isNumber", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            sys.push(numberCheck.isNumber(str));
        }
    )],
    ["toList", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.STRING],
            function (sys, str) {
                var res = [];
                str = str;
                for (var i = 0; i < str.length; i++) {
                    res.push(str.charAt(i))
                }
                sys.push(res);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.COMMAND],
            function (sys, com) {
                sys.push(com.toList());
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.MULTI_FUNCTION],
            function (sys, mfunc) {
                sys.push(mfunc.toList());
            }
        )
    ])],
    ["toChar", new SRLNativeFunction(
        [nativeTypes.NUMBER],
        function (sys, n) {
            sys.push(String.fromCharCode(n));
        }
    )],
    ["toCharCode", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, s) {
            sys.push(s.charCodeAt(0));
        }
    )],
    ["split", new SRLNativeFunction(
        [nativeTypes.STRING, nativeTypes.STRING],
        function (sys, s1, s2) {
            sys.push(s1.split(s2));
        }
    )],
    ["invert", new SRLNativeFunction(
        [nativeTypes.LIST],
        function (sys, list) {
            var res = [];
            for (var i = list.length - 1; i >= 0; i--) {
                res.push(list[i]);
            }
            sys.push(res);
        }
    )],
    ["at", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.LIST, nativeTypes.NUMBER],
            function (sys, list, pos) {
                if (pos < 0) {
                    pos = list.length + pos;
                }
                if (pos < list.length) {
                    sys.push(list[pos]);
                } else {
                    sys.throwCurrentTokenError("Invalid List Position: " + pos);
                }
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.LIST, nativeTypes.LIST],
            function (sys, list, posList) {
                var res = [];
                var hasError = false;
                for (var pos of posList) {
                    if (typeof pos == "number") {
                        if (pos < 0) {
                            pos = list.length + pos;
                        }
                        if (pos < list.length) {
                            res.push(list[pos])
                        } else {
                            sys.throwCurrentTokenError("Invalid List Position: " + pos);
                            hasError = true;
                            break;
                        }
                    } else {
                        sys.throwCurrentTokenError("The positions must be number values.");
                        hasError = true;
                        break;
                    }
                }
                if (!hasError) {
                    sys.push(res);
                }
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.NUMBER],
            function (sys, str, pos) {
                if (pos < 0) {
                    pos = str.length + pos;
                }
                if (pos < str.length) {
                    sys.push(str.charAt(pos));
                } else {
                    sys.throwCurrentTokenError("Invalid string character Position");
                }
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.LIST],
            function (sys, str, posList) {
                var res = "";
                var hasError = false;
                for (var pos of posList) {
                    if (typeof pos == "number") {
                        if (pos < 0) {
                            pos = str.length + pos;
                        }
                        if (pos < str.length) {
                            res += str.charAt(pos);
                        } else {
                            sys.throwCurrentTokenError("Invalid string character Position: " + pos);
                            hasError = true;
                            break;
                        }
                    } else {
                        sys.throwCurrentTokenError("The positions must be number values.");
                        hasError = true;
                        break;
                    }
                }
                if (!hasError) {
                    sys.push(res);
                }
            }
        )
    ])],
    ["set", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.LIST, nativeTypes.NUMBER, nativeTypes.ANY],
            function (sys, list, pos, a) {
                if (pos < 0) {
                    pos = list.length + pos;
                }
                var res = [];
                if (pos < list.length) {
                    for (var i = 0; i < pos; i++) {
                        res.push(list[i]);
                    }
                    res.push(a);
                    for (var i = pos + 1; i < list.length; i++) {
                        res.push(list[i]);
                    }
                    sys.push(res);
                } else if (pos == list.length) {
                    for (var i = 0; i < list.length; i++) {
                        res.push(list[i]);
                    }
                    res.push(a);
                    sys.push(res);
                } else {
                    sys.throwCurrentTokenError("Invalid List Position: " + pos);
                }
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.LIST, nativeTypes.LIST, nativeTypes.LIST],
            function (sys, list, posList, valList) {
                if (posList.length >= valList.length) {
                    var res = utils.copyList(list);
                    var hasError = false;
                    for (var i = 0; i < posList.length; i++) {
                        var pos = posList[i]
                        if (typeof pos == "number") {
                            if (pos < 0) {
                                pos = list.length + pos;
                            }
                            if (pos < list.length) {
                                res[pos] = valList[i];
                            } else {
                                sys.throwCurrentTokenError("Invalid List Position: " + pos);
                                hasError = true;
                                break;
                            }
                        } else {
                            sys.throwCurrentTokenError("The positions must be number values.");
                            hasError = true;
                            break;
                        }
                    }
                    if (!hasError) {
                        sys.push(res);
                    }
                } else {
                    sys.throwCurrentTokenError("Insufficient values for the positions.");
                }

            }
        )
    ])],
    ["listPush", new SRLNativeFunction(
        [nativeTypes.LIST, nativeTypes.ANY],
        function (sys, list, a) {
            var l = utils.copyList(list);
            l.push(a);
            sys.push(l);
        }
    )],
    ["listPop", new SRLNativeFunction(
        [nativeTypes.LIST],
        function (sys, list) {
            if (list.length) {
                var l = utils.copyList(list);
                l.pop();
                sys.push(l);
            } else {
                sys.throwCurrentTokenError("Empty List");
            }
        }
    )],
    ["cut", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.LIST, nativeTypes.NUMBER],
            function (sys, list, pos) {
                if (pos < 0) {
                    pos = list.length + pos;
                }
                if (pos > list.length) {
                    sys.throwCurrentTokenError("Invalid cut position.")
                } else {
                    var first = [];
                    var last = [];
                    var i;
                    for (i = 0; i < pos; i++) {
                        first.push(list[i]);
                    }
                    for (; i < list.length; i++) {
                        last.push(list[i]);
                    }
                    sys.push(first);
                    sys.push(last);
                }
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.NUMBER],
            function (sys, str, pos) {
                if (pos < 0) {
                    pos = str.length + pos;
                }
                if (pos > str.length) {
                    sys.throwCurrentTokenError("Invalid cut position.")
                }
                sys.push(str.substring(0, pos));
                sys.push(str.substring(pos));
            }
        )
    ])],
    ["replace", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.STRING, nativeTypes.STRING],
            function (sys, s1, s2, s3) {
                sys.push(utils.stringReplace(s1, s2, s3));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.COMMAND, nativeTypes.STRING, nativeTypes.COMMAND],
            function (sys, com1, str, com2) {
                sys.push(com1.replace(str, com2));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.FUNCTION, nativeTypes.STRING, nativeTypes.COMMAND],
            function (sys, func, str, com) {
                sys.push(func.replace(str, com));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.MULTI_FUNCTION, nativeTypes.STRING, nativeTypes.COMMAND],
            function (sys, mfunc, str, com) {
                sys.push(mfunc.replace(str, com));
            }
        )
    ])],
    ["time", new SRLNativeFunction(
        [],
        function (sys) {
            var d = new Date();
            sys.push(d.getTime());
        }
    )],
    ["timeList", new SRLNativeFunction(
        [],
        function (sys) {
            var d = new Date();
            sys.push([d.getFullYear(), d.getMonth() + 1, d.getDate(), d.getDay(), d.getTime(), d.getHours(), d.getMinutes(), d.getSeconds()]);
        }
    )],
    ["run", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.COMMAND],
            function (sys, com) {
                sys.processCommand(com);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.FUNCTION],
            function (sys, func) {
                func.checkAndRun(sys);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.NATIVE_FUNCTION],
            function (sys, nfunc) {
                nfunc.checkAndRun(sys);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.MULTI_FUNCTION],
            function (sys, mfunc) {
                mfunc.checkAndRun(sys);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING],
            function (sys, str) {
                try {
                    sys.processCommand(new SRLCommand(sys.parse(str)));
                } catch (e) {
                    sys.throwCurrentTokenError(e)
                }
            }
        )
    ])],
    ["parse", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            try {
                sys.push(new SRLCommand(sys.parse(str)));
            } catch (e) {
                sys.throwCurrentTokenError(e)
            }
        }
    )],
    ["pop", new SRLNativeFunction(
        [nativeTypes.ANY],
        function () { }
    )],
    ["double", new SRLNativeFunction(
        [nativeTypes.ANY],
        function (sys, a) {
            sys.push(a);
            sys.push(a);
        }
    )],
    ["flip", new SRLNativeFunction(
        [nativeTypes.ANY, nativeTypes.ANY],
        function (sys, a1, a2) {
            sys.push(a2);
            sys.push(a1);
        }
    )],
    ["<<", new SRLNativeFunction(
        [],
        function (sys) {
            sys.rotl();
        }
    )],
    [">>", new SRLNativeFunction([],
        function (sys) {
            sys.rotr();
        }
    )],
    ["dump", new SRLNativeFunction(
        [],
        function (sys) {
            sys.push(sys.dumpStack());
        }
    )],
    ["Console.clear", new SRLNativeFunction(
        [],
        function (sys) {
            sys.consoleSystem.clear();
        }
    )],
    ["Console.print", new SRLNativeFunction(
        [nativeTypes.ANY],
        function (sys, a) {
            sys.consoleSystem.print(sys.valueToString(a));
        }
    )],
    ["Console.printWarning", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            sys.consoleSystem.warning(str);
        }
    )],
    ["Console.printError", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            sys.consoleSystem.error(str);
        }
    )],
    ["Console.printStack", new SRLNativeFunction(
        [],
        function (sys) {
            sys.consoleSystem.print(sys.generateStackString());
        }
    )],
    ["Console.dimensions", new SRLNativeFunction(
        [],
        function (sys) {
            sys.push(sys.consoleSystem.dimensions());
        }
    )],
    ["throw", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            sys.throwCurrentTokenError(str);
        }
    )],
    ["Console.key", new SRLNativeFunction(
        [],
        function (sys) {
            sys.hold();
            sys.consoleSystem.getPriorityKey(function (key) {
                sys.push(key);
                sys.run();
            });
        }
    )],
    ["Console.aKey", new SRLNativeFunction(
        [nativeTypes.COMMAND],
        function (sys, com) {
            sys.addAsyncRequest();
            sys.consoleSystem.getKey(function (key) {
                sys.loadAsyncResult(function () { sys.push(key) }, com);
            });
        }
    )],
    ["Console.keyWait", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.NUMBER],
        function (sys, com, time) {
            sys.hold();
            var t;
            var callback = function (key) {
                clearTimeout(t);
                sys.push(key);
                sys.processCommand(com);
                sys.run();
            };
            sys.consoleSystem.getPriorityKey(callback);
            t = setTimeout(function () {
                sys.consoleSystem.removeGetKey(callback);
                sys.run();
            }, time)
        }
    )],
    ["wait", new SRLNativeFunction(
        [nativeTypes.NUMBER],
        function (sys, time) {
            if (time > 0) {
                sys.hold();
                setTimeout(function () {
                    sys.run();
                }, time);
            } else {
                sys.throwCurrentTokenError("Waiting time must be positive");
            }
        }
    )],
    ["aWait", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.NUMBER],
        function (sys, com, time) {
            if (time >= 0) {
                sys.addAsyncRequest();
                setTimeout(function () {
                    sys.loadAsyncResult(function () { }, com);
                }, time);
            } else {
                sys.throwCurrentTokenError("Waiting time must be non-negative");
            }
        }
    )],
    ["isFile", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            sys.push(sys.fileSystem.isFile(str));
        }
    )],
    ["readFile", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            try {
                sys.push(sys.fileSystem.readFile(str).toString());
            } catch (e) {
                sys.throwCurrentTokenError("Unable to read file \"" + str + "\".")
            }
        }
    )],
    ["writeFile", new SRLNativeFunction(
        [nativeTypes.STRING, nativeTypes.STRING],
        function (sys, text, path) {
            try {
                sys.push(sys.fileSystem.writeFile(text, path));
            } catch (e) {
                sys.throwCurrentTokenError("Unable to write file \"" + path + "\".")
            }
        }
    )],
    ["removeFile", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            try {
                sys.push(sys.fileSystem.removeFile(str));
            } catch (e) {
                sys.throwCurrentTokenError("Unable to remove file \"" + str + "\".")
            }
        }
    )],
    ["load", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.COMMAND],
            function (sys, com) {
                sys.loadOnRoot(com);
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING],
            function (sys, str) {
                try {
                    sys.loadOnRoot(new SRLCommand(sys.parse(str)));
                } catch (e) {
                    sys.throwCurrentTokenError(e)
                }
            }
        )
    ])],
    ["isDir", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            sys.push(sys.fileSystem.isDir(str));
        }
    )],
    ["listDir", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            try {
                sys.push(sys.fileSystem.listDir(str));
            } catch (e) {
                sys.throwCurrentTokenError("Unable to list directory \"" + str + "\".")
            }
        }
    )],
    ["makeDir", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            try {
                sys.push(sys.fileSystem.makeDir(str));
            } catch (e) {
                sys.throwCurrentTokenError("Unable to make directory \"" + str + "\".")
            }
        }
    )],
    ["removeDir", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            try {
                sys.push(sys.fileSystem.removeDir(str));
            } catch (e) {
                sys.throwCurrentTokenError("Unable to remove directory \"" + str + "\".")
            }
        }
    )],
    ["getCWD", new SRLNativeFunction(
        [],
        function (sys) {
            sys.push(sys.getCWD());
        }
    )],
    ["setCWD", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            try {
                sys.setCWD(str);
            } catch (e) {
                sys.throwCurrentTokenError("Invalid directory \"" + str + "\".")
            }
        }
    )],
    ["resolvePath", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, str) {
            try {
                sys.push(sys.resolvePath(str));
            } catch (e) {
                sys.throwCurrentTokenError("Invalid Path \"" + str + "\".")
            }
        }
    )],
    ["random", new SRLNativeFunction(
        [],
        function (sys) {
            sys.push(Math.random());
        }
    )],
    ["onRootError", new SRLNativeFunction(
        [nativeTypes.COMMAND],
        function (sys, com) {
            sys.addRootErrorCommand(com);
        }
    )],
    ["func", new SRLNativeFunction(
        [nativeTypes.LIST, nativeTypes.COMMAND],
        function (sys, list, com) {
            var hasError = false;
            for (var p of list) {
                if (!sys.isValidType(p)) {
                    hasError = true;
                    break;
                }
            }
            if (hasError) {
                sys.throwCurrentTokenError("Invalid type found on the list.");
            } else {
                sys.push(new SRLFunction(list, com));
            }
        }
    )],
    ["unFunc", new SRLNativeFunction(
        [nativeTypes.FUNCTION],
        function (sys, func) {
            sys.push(func.paramList());
            sys.push(func.toCommand());
        }
    )],
    ["mfunc", new SRLNativeFunction(
        [nativeTypes.LIST],
        function (sys, list) {
            var hasError = false;
            for (var f of list) {
                if (!(f instanceof SRLFunction || f instanceof SRLNativeFunction)) {
                    hasError = true;
                    break;
                }
            }
            if (hasError) {
                sys.throwCurrentTokenError("Non function value found on the list.");
            } else {
                sys.push(new SRLMultiFunction(list));
            }
        }
    )],
    ["toTokenList", new SRLNativeFunction(
        [nativeTypes.COMMAND],
        function (sys, com) {
            sys.push(com.toTokenList());
        }
    )],
    ["toCommand", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.FUNCTION],
            function (sys, func) {
                sys.push(func.toCommand());
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.NUMBER],
            function (sys, num) {
                sys.push(new SRLCommand([{ token: tokenType.NUMBER, value: num, toString: tokenStrings.NUMBER }]));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.STRING],
            function (sys, str) {
                sys.push(new SRLCommand([{ token: tokenType.NUMBER, value: str, toString: tokenStrings.STRING }]));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.BOOLEAN],
            function (sys, bool) {
                sys.push(new SRLCommand([{ token: tokenType.BOOLEAN, value: bool, toString: tokenStrings.BOOLEAN }]));
            }
        )
    ])],
    ["rename", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.COMMAND, nativeTypes.STRING, nativeTypes.STRING],
            function (sys, com, oldName, newName) {
                sys.push(com.rename(oldName, newName));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.FUNCTION, nativeTypes.STRING, nativeTypes.STRING],
            function (sys, func, oldName, newName) {
                sys.push(func.rename(oldName, newName));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.MULTI_FUNCTION, nativeTypes.STRING, nativeTypes.STRING],
            function (sys, mfunc, oldName, newName) {
                sys.push(mfunc.rename(oldName, newName));
            }
        )
    ])],
    ["registerType", new SRLNativeFunction(
        [nativeTypes.STRING, nativeTypes.COMMAND, nativeTypes.COMMAND],
        function (sys, name, checkCom, toStringCom) {
            try {
                var aux = sys.parse(name)
                if (aux.length == 1 && aux[0].token == tokenType.VAR) {
                    sys.registerType(name, checkCom, toStringCom);
                } else {
                    sys.throwCurrentTokenError("The type name is not valid");
                }
            } catch{
                sys.throwCurrentTokenError("The type name is not valid");
            }
        }
    )],
    ["new", new SRLNativeFunction(
        [nativeTypes.ANY, nativeTypes.TYPE],
        function (sys, a, type) {
            if (type instanceof CustomType) {
                sys.pushCustomTypeValue(type, a);
            } else {
                sys.throwCurrentTokenError("The given type must be a custom type.");
            }

        }
    )],
    ["value", new SRLNativeFunction(
        [nativeTypes.ANY],
        function (sys, a) {
            if (a instanceof CustomTypeInstance) {
                sys.push(a.value);
            } else {
                sys.throwCurrentTokenError("The given value is not a custom type value");
            }
        }
    )],
    ["VM.setBaseImage", new SRLNativeFunction(
        [],
        function (sys) {
            sys.setVMBaseImage();
        }
    )],
    ["VM.new", new SRLNativeFunction(
        [],
        function (sys) {
            sys.push(sys.newVM());
        }
    )],
    ["VM.run", new SRLMultiFunction([
        new SRLNativeFunction(
            [nativeTypes.STRING, nativeTypes.STRING],
            function (sys, str, key) {
                sys.runVM(key, new SRLCommand(sys.parse(str)));
            }
        ),
        new SRLNativeFunction(
            [nativeTypes.COMMAND, nativeTypes.STRING],
            function (sys, com, key) {
                sys.runVM(key, com);
            }
        )
    ])],
    ["VM.dumpStack", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, key) {
            sys.push(sys.dumpVMStack(key));
        }
    )],
    ["VM.setStack", new SRLNativeFunction(
        [nativeTypes.List, nativeTypes.STRING],
        function (sys, stack, key) {
            sys.setVMStack(key, utils.copyList(stack));
        }
    )],
    ["VM.getError", new SRLNativeFunction(
        [nativeTypes.STRING],
        function (sys, key) {
            sys.push(sys.getVMError(key));
        }
    )],
    ["varList", new SRLNativeFunction(
        [],
        function (sys) {
            sys.push(sys.getVarList());
        }
    )],
    ["gui.start", new SRLNativeFunction(
        [],
        function (sys) {
            if (!guiServerStarted) {
                http.createServer(function (request, response) {
                    response.writeHead(200, { 'Content-type': 'text/html' });
                    response.write(page);
                    response.end();
                }).listen(7000);
                ws.createServer(function (c) {
                    sys.guiSystem.setConnection(c)
                }).listen(8001);
            }
            guiServerStarted = true;
        }
    )],
    ["gui.setColor", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, r, g, b) {
            sys.guiSystem.setColor(r, g, b)
        }
    )],
    ["gui.setBackgroundColor", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, r, g, b) {
            sys.guiSystem.setBackgroundColor(r, g, b)
        }
    )],
    ["gui.drawPoint", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, x, y) {
            sys.guiSystem.drawPoint(x, y)
        }
    )],
    ["gui.drawFRect", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER, nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, x, y, width, height) {
            sys.guiSystem.drawFRect(x, y, width, height)
        }
    )],
    ["gui.drawSprite", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, x, y, spriteId) {
            sys.guiSystem.drawSprite(x, y, spriteId);
        }
    )],
    ["gui.drawScaledSprite", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER, nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, x, y, spriteId, scale) {
            sys.guiSystem.drawScaledSprite(x, y, spriteId, scale)
        }
    )],
    ["gui.render", new SRLNativeFunction(
        [],
        function (sys) {
            sys.guiSystem.render()
        }
    )],
    ["gui.clear", new SRLNativeFunction(
        [],
        function (sys) {
            sys.guiSystem.clear()
        }
    )],
    ["gui.setLimit", new SRLNativeFunction(
        [nativeTypes.NUMBER, nativeTypes.NUMBER],
        function (sys, x, y) {
            sys.guiSystem.setLimit(x, y)
        }
    )],
    ["gui.setCenter", new SRLNativeFunction(
        [nativeTypes.BOOLEAN],
        function (sys, center) {
            sys.guiSystem.setCenter(center)
        }
    )],
    ["gui.dimensions", new SRLNativeFunction(
        [],
        function (sys) {
            sys.push(sys.guiSystem.getDimensions());
        }
    )],
    ["gui.loadSprite", new SRLNativeFunction(
        [nativeTypes.LIST],
        function (sys, sprite) {
            sys.push(sys.guiSystem.loadSprite(sprite));
        }
    )],
    ["gui.dropSprites", new SRLNativeFunction(
        [],
        function (sys) {
            sys.guiSystem.dropSprites();
        }
    )],
    ["gui.key", new SRLNativeFunction(
        [],
        function (sys) {
            sys.hold();
            sys.guiSystem.getPriorityKey(function (key) {
                sys.push(key);
                sys.run();
            });
        }
    )],
    ["gui.aKey", new SRLNativeFunction(
        [nativeTypes.COMMAND],
        function (sys, com) {
            sys.addAsyncRequest();
            sys.guiSystem.getKey(function (key) {
                sys.loadAsyncResult(function () { sys.push(key) }, com);
            });
        }
    )],
    ["gui.keyWait", new SRLNativeFunction(
        [nativeTypes.COMMAND, nativeTypes.NUMBER],
        function (sys, command, time) {
            sys.hold();
            var t;
            var callback = function (key) {
                clearTimeout(t);
                sys.push(key);
                sys.processCommand(command);
                sys.run();
            }
            sys.guiSystem.getPriorityKey(callback);
            t = setTimeout(function () {
                sys.guiSystem.removeGetKey(callback);
                sys.run();
            }, time)
        }
    )],
    ["gui.mouseClick", new SRLNativeFunction(
        [nativeTypes.COMMAND],
        function (sys, com) {
            sys.addAsyncRequest();
            sys.guiSystem.getMouseClick(function (pos) {
                sys.loadAsyncResult(function () { sys.push(pos) }, com);
            });
        }
    )],
    ["gui.mouseMove", new SRLNativeFunction(
        [nativeTypes.COMMAND],
        function (sys, com) {
            sys.addAsyncRequest();
            sys.guiSystem.getMouseMove(function (pos) {
                sys.loadAsyncResult(function () { sys.push(pos) }, com);
            });
        }
    )]
]