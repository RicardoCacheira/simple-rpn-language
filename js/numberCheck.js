module.exports.isInt = function (num) {
    num.trim();
    if (num.length) {
        var i = 0;
        var aux;
        if (num.charAt(0) == "-") {
            if (num.length > 1) {
                i = 1;
            } else {
                return false;
            }
        }
        for (; i < num.length; i++) {
            aux = num.charCodeAt(i);
            if (aux < 48 || aux > 57) {
                return false;
            }
        }
        return true;
    }
    return false;
}

module.exports.isNumber = function (num) {
    num.trim();
    if (num.length) {
        var i = 0;
        var aux;
        var hasNoDot = true;
        if (num.charAt(0) == "-") {
            if (num.length > 1) {
                i = 1;
            } else {
                return false;
            }
        }
        for (; i < num.length; i++) {
            aux = num.charCodeAt(i);
            if (aux < 48 || aux > 57) {
                if (hasNoDot) {
                    if (aux == 46) {
                        hasNoDot = false;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        }
        return true;
    }
    return false;
}