const constants = require("./constants");
const numberCheck = require("./numberCheck");
const utils = require("./utils");
const tokenType = require("./tokenType");
const tokenStrings = require("./tokenStrings");
const SRLCommand = require("./srlCommand");

function invalidVarCharPos(str) {
    var aux;
    for (var i = 0; i < str.length; i++) {
        aux = str.charAt(i);
        if (aux == constants.TOKEN_LIST_START ||
            aux == constants.TOKEN_LIST_END ||
            aux == constants.TOKEN_COMMAND_START ||
            aux == constants.TOKEN_COMMAND_END ||
            aux == constants.TOKEN_BLOCK_START ||
            aux == constants.TOKEN_BLOCK_END ||
            aux == constants.TOKEN_SET_VAR ||
            aux == constants.TOKEN_GET_VAR ||
            aux == constants.TOKEN_TOP_VAR ||
            aux == constants.TOKEN_LOCAL_VAR ||
            aux == constants.TOKEN_STRING_CHAR) {
            return { char: aux, pos: i };
        }
    }
}

function isWhitespace(char) {
    return char == " " || (char == "\n" || char == "\t" || char == ",");
}

function throwParserError(error) {
    if (error.token) {
        throw "Error on line " + error.line + ": " + error.msn + "\n\n" + utils.repeatChar(" ", 4) + error.token + "\n" + utils.repeatChar(" ", 4 + error.start) + utils.repeatChar("^", error.end - error.start);
    } else {
        throw "Error on line " + error.line + ": " + error.msn + "\n";
    }
}

class SRLParser {

    constructor() {
        this.localVarId = 0;
    }

    getLocalVarId() {
        var v = this.localVarId;
        this.localVarId++;
        return v;
    }

    parse(code, line = 1) {
        var tokens = [];
        var isReading = false;
        var pos = 0;
        var lineCount = line;
        var char;
        var start;
        var aux;
        var buffer;
        var check;
        var listCount = 0;
        var blockList = [];
        var currBlock = new Map();
        var commandStartList = [];
        var commandStartPosList = [];
        var codeName = "system"
        var parser = this;
        function varId(varName) {
            if (!currBlock.has(varName)) {
                currBlock.set(varName, parser.getLocalVarId());
            }
            return currBlock.get(varName);
        }
        //the added space is an hack to keep me from copying the code that checks what the last token is after the main
        code = code.replace(/\r/g, "") + " ";
        while (pos < code.length) {
            char = code.charAt(pos);
            if (isReading) {
                //Stop reading
                if (isWhitespace(char) || char == constants.TOKEN_COMMENT_CHAR) {
                    aux = code.substring(start, pos);
                    switch (aux) {
                        case constants.TOKEN_TRUE:
                            tokens.push({ token: tokenType.BOOLEAN, value: true, start: start, end: pos, toString: tokenStrings.BOOLEAN });
                            break;
                        case constants.TOKEN_FALSE:
                            tokens.push({ token: tokenType.BOOLEAN, value: false, start: start, end: pos, toString: tokenStrings.BOOLEAN });
                            break;
                        case constants.TOKEN_EXIT:
                            tokens.push({ token: tokenType.EXIT, start: start, end: pos, toString: tokenStrings.EXIT });
                            break;
                        case constants.TOKEN_LIST_START:
                            listCount++;
                            tokens.push({ token: tokenType.LIST_START, start: start, end: pos, toString: tokenStrings.LIST_START, codeName: codeName });
                            break;
                        case constants.TOKEN_LIST_END:
                            listCount--;
                            if (listCount < 0) {
                                throwParserError({ msn: "Ending non-existant list.", line: lineCount, start: 0, end: 1, token: constants.TOKEN_LIST_END });
                            }
                            tokens.push({ token: tokenType.LIST_END, start: start, end: pos, toString: tokenStrings.LIST_END, codeName: codeName });
                            break;
                        case constants.TOKEN_BLOCK_START:
                            blockList.push(currBlock);
                            currBlock = new Map();
                            tokens.push({ token: tokenType.BLOCK_START, start: start, end: pos, toString: tokenStrings.BLOCK_START, codeName: codeName });
                            break;
                        case constants.TOKEN_BLOCK_END:
                            if (blockList.length == 0) {
                                throwParserError({ msn: "Ending non-existant block.", line: lineCount, start: 0, end: 1, token: constants.TOKEN_BLOCK_END });
                            }
                            currBlock = blockList.pop();
                            tokens.push({ token: tokenType.BLOCK_END, start: start, end: pos, toString: tokenStrings.BLOCK_END, codeName: codeName });
                            break;
                        case constants.TOKEN_COMMAND_START:
                            commandStartList.push(tokens.length);
                            commandStartPosList.push(start);
                            break;
                        case constants.TOKEN_COMMAND_END:
                            if (commandStartList.length < 0) {
                                throwParserError({ msn: "Ending non-existant command.", line: lineCount, start: 0, end: 1, token: constants.TOKEN_COMMAND_END });
                            }
                            var start = commandStartList.pop();
                            tokens.push({ token: tokenType.COMMAND, value: new SRLCommand(tokens.splice(start, tokens.length - start)), start: commandStartPosList.pop(), end: pos, line: lineCount, toString: tokenStrings.COMMAND, codeName: codeName });
                            break;
                        default:
                            if (numberCheck.isNumber(aux)) {
                                tokens.push({ token: tokenType.NUMBER, value: Number.parseFloat(aux), start: start, end: pos, toString: tokenStrings.NUMBER });
                            } else {
                                switch (aux.charAt(0)) {
                                    case constants.TOKEN_SET_VAR:
                                        if (aux.length > 1) {
                                            aux = aux.substring(1);
                                            switch (aux.charAt(0)) {
                                                case constants.TOKEN_TOP_VAR:
                                                    if (aux.length > 1) {
                                                        aux = aux.substring(1);
                                                        check = invalidVarCharPos(aux);
                                                        if (check) {
                                                            throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                                        } else {
                                                            tokens.push({ token: tokenType.SET_TOP_VAR, value: aux, start: start, end: pos, line: lineCount, toString: tokenStrings.SET_TOP_VAR, codeName: codeName });
                                                        }
                                                    } else {
                                                        throwParserError({ msn: "No variable name after the Top Variable character.", line: lineCount, start: 1, end: 2, token: constants.TOKEN_SET_VAR + constants.TOKEN_TOP_VAR });
                                                    }
                                                    break;
                                                case constants.TOKEN_LOCAL_VAR:
                                                    if (aux.length > 1) {
                                                        aux = aux.substring(1);
                                                        check = invalidVarCharPos(aux);
                                                        if (check) {
                                                            throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                                        } else {
                                                            if (currBlock) {
                                                                tokens.push({ token: tokenType.SET_LOCAL_VAR, value: varId(aux), start: start, end: pos, line: lineCount, toString: tokenStrings.SET_LOCAL_VAR, codeName: codeName, name: aux });
                                                            } else {
                                                                throwParserError({ msn: "Local Variable outside a block.", line: lineCount, start: 0, end: aux.length + 2, token: constants.TOKEN_SET_VAR + constants.TOKEN_LOCAL_VAR + aux });
                                                            }
                                                        }
                                                    } else {
                                                        throwParserError({ msn: "No variable name after the Local Variable character.", line: lineCount, start: 1, end: 2, token: constants.TOKEN_SET_VAR + constants.TOKEN_LOCAL_VAR });
                                                    }
                                                    break;
                                                default:
                                                    check = invalidVarCharPos(aux)
                                                    if (check) {
                                                        throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                                    } else {
                                                        tokens.push({ token: tokenType.SET_VAR, value: aux, start: start, end: pos, line: lineCount, toString: tokenStrings.SET_VAR, codeName: codeName });
                                                    }
                                            }
                                        } else {
                                            throwParserError({ msn: "No variable name after the Set Variable character.", line: lineCount, start: 0, end: 1, token: constants.TOKEN_SET_VAR });
                                        }
                                        break;
                                    case constants.TOKEN_GET_VAR:
                                        if (aux.length > 1) {
                                            aux = aux.substring(1);
                                            switch (aux.charAt(0)) {
                                                case constants.TOKEN_TOP_VAR:
                                                    if (aux.length > 1) {
                                                        aux = aux.substring(1);
                                                        check = invalidVarCharPos(aux);
                                                        if (check) {
                                                            throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                                        } else {
                                                            tokens.push({ token: tokenType.GET_TOP_VAR, value: aux, start: start, end: pos, line: lineCount, toString: tokenStrings.GET_TOP_VAR, codeName: codeName });
                                                        }
                                                    } else {
                                                        throwParserError({ msn: "No variable name after the Top Variable character.", line: lineCount, start: 1, end: 2, token: constants.TOKEN_GET_VAR + constants.TOKEN_TOP_VAR });
                                                    }
                                                    break;
                                                case constants.TOKEN_LOCAL_VAR:
                                                    if (aux.length > 1) {
                                                        aux = aux.substring(1);
                                                        check = invalidVarCharPos(aux);
                                                        if (check) {
                                                            throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                                        } else {
                                                            if (currBlock) {
                                                                tokens.push({ token: tokenType.GET_LOCAL_VAR, value: varId(aux), start: start, end: pos, line: lineCount, toString: tokenStrings.GET_LOCAL_VAR, codeName: codeName, name: aux });
                                                            } else {
                                                                throwParserError({ msn: "Local Variable outside a block.", line: lineCount, start: 0, end: aux.length + 2, token: constants.TOKEN_GET_VAR + constants.TOKEN_LOCAL_VAR + aux });
                                                            }
                                                        }
                                                    } else {
                                                        throwParserError({ msn: "No variable name after the Local Variable character.", line: lineCount, start: 1, end: 2, token: constants.TOKEN_GET_VAR + constants.TOKEN_LOCAL_VAR });
                                                    }
                                                    break;
                                                default:
                                                    check = invalidVarCharPos(aux)
                                                    if (check) {
                                                        throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                                    } else {
                                                        tokens.push({ token: tokenType.GET_VAR, value: aux, start: start, end: pos, line: lineCount, toString: tokenStrings.GET_VAR, codeName: codeName });
                                                    }
                                            }
                                        } else {
                                            throwParserError({ msn: "No variable name after the Get Variable character.", line: lineCount, start: 0, end: 1, token: constants.TOKEN_GET_VAR });
                                        }
                                        break;
                                    case constants.TOKEN_TOP_VAR:
                                        if (aux.length > 1) {
                                            aux = aux.substring(1);
                                            check = invalidVarCharPos(aux);
                                            if (check) {
                                                throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                            } else {
                                                tokens.push({ token: tokenType.TOP_VAR, value: aux, start: start, end: pos, line: lineCount, toString: tokenStrings.TOP_VAR, codeName: codeName });
                                            }
                                        } else {
                                            throwParserError({ msn: "No variable name after the Top Variable character.", line: lineCount, start: 0, end: 1, token: constants.TOKEN_TOP_VAR });
                                        }
                                        break;
                                    case constants.TOKEN_LOCAL_VAR:
                                        if (aux.length > 1) {
                                            aux = aux.substring(1);
                                            check = invalidVarCharPos(aux);
                                            if (check) {
                                                throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                            } else {
                                                if (currBlock) {
                                                    tokens.push({ token: tokenType.LOCAL_VAR, value: varId(aux), start: start, end: pos, line: lineCount, toString: tokenStrings.LOCAL_VAR, codeName: codeName, name: aux });
                                                } else {
                                                    throwParserError({ msn: "Local Variable outside a block.", line: lineCount, start: 0, end: aux.length + 1, token: constants.TOKEN_LOCAL_VAR + aux });
                                                }
                                            }
                                        } else {
                                            throwParserError({ msn: "No variable name after the Local Variable character.", line: lineCount, start: 0, end: 1, token: constants.TOKEN_LOCAL_VAR });
                                        }
                                        break;
                                    default:
                                        check = invalidVarCharPos(aux)
                                        if (check) {
                                            throwParserError({ msn: "Invalid character '" + check.char + "' in variable name.", line: lineCount, start: check.pos, end: check.pos + 1, token: aux });
                                        } else {
                                            tokens.push({ token: tokenType.VAR, value: aux, start: start, end: pos, line: lineCount, toString: tokenStrings.VAR, codeName: codeName });
                                        }
                                        break;
                                }
                            }
                    }
                    isReading = false;
                    //makes that the comment start is caught in the next loop
                    if (char == constants.TOKEN_COMMENT_CHAR) {
                        pos--;
                    }
                }

            } else if (!isWhitespace(char)) {
                //Starts reading a string
                if (char == constants.TOKEN_STRING_CHAR) {
                    buffer = "";
                    start = pos;
                    pos++;
                    char = code.charAt(pos);
                    aux = []
                    while (pos < code.length && char != constants.TOKEN_STRING_CHAR) {
                        if (char == "\\") {
                            pos++;
                            if (pos < code.length) {
                                switch (code.charAt(pos)) {
                                    case "n":
                                        buffer += "\n";
                                        break;
                                    case "t":
                                        buffer += "\t";
                                        break;
                                    case "r":
                                        buffer += "\r";
                                        break;
                                    case "e":
                                        buffer += "\u001b";
                                        break;
                                    case constants.TOKEN_STRING_CHAR:
                                        buffer += constants.TOKEN_STRING_CHAR;
                                        break;
                                    case "\\":
                                        buffer += "\\";
                                        break;
                                    case constants.TOKEN_COMMAND_START:
                                        aux.push(buffer);
                                        buffer = "";
                                        pos++;
                                        char = code.charAt(pos);
                                        var stringCommandStart = pos;
                                        while (pos < code.length && char != constants.TOKEN_COMMAND_END) {
                                            pos++;
                                            char = code.charAt(pos);
                                        }
                                        if (pos < code.length) {
                                            aux.push(this.parse(code.substring(stringCommandStart, pos), lineCount))
                                        } else {
                                            throwParserError({ msn: "Open string command definition.", line: lineCount });
                                        }
                                        break;
                                    default:
                                        throwParserError({ msn: "Invalid escape character '" + code.charAt(pos) + "' in string.", line: lineCount, start: pos - start - 1, end: pos + 1 - start, token: code.substring(start, pos + 1) });
                                }
                            } else {
                                throwParserError({ msn: "Unfinished string.", line: lineCount });
                            }
                        } else {
                            if (char == '\n') {
                                lineCount++;
                            }
                            buffer += char;
                        }
                        pos++;
                        char = code.charAt(pos);
                    }
                    if (pos < code.length) {
                        if (pos + 1 == code.length || (isWhitespace(code.charAt(pos + 1)) || code.charAt(pos + 1) == constants.TOKEN_COMMENT_CHAR)) {
                            if (aux.length) {
                                aux.push(buffer);
                                tokens.push({ token: tokenType.STRING_COMMAND, value: aux, start: start, end: pos + 1, toString: tokenStrings.STRING_COMMAND, codeName: codeName });
                            } else {
                                tokens.push({ token: tokenType.STRING, value: buffer, start: start, end: pos + 1, toString: tokenStrings.STRING });
                            }
                        } else {
                            throwParserError({ msn: "Invalid character '" + code.charAt(pos + 1) + "' after string end.", line: lineCount, start: pos - start + 1, end: pos - start + 2, token: code.substring(start, pos + 2) });
                        }
                    } else {
                        throwParserError({ msn: "Unfinished string.", line: lineCount });
                    }
                } else if (char == constants.TOKEN_COMMENT_CHAR) {
                    start = pos;
                    pos++
                    char = code.charAt(pos);
                    //Is a comment block
                    if (char == constants.TOKEN_COMMENT_BLOCK_CHAR) {
                        var blockCount = 0;
                        while (pos < code.length) {
                            if (char == '\n') {
                                lineCount++;
                            } else if (char == constants.TOKEN_COMMENT_CHAR) {
                                pos++;
                                char = code.charAt(pos);
                                if (char == constants.TOKEN_COMMENT_BLOCK_CHAR) {
                                    blockCount++;
                                }
                            } else if (char == constants.TOKEN_COMMENT_BLOCK_CHAR) {
                                pos++;
                                char = code.charAt(pos);
                                if (char == constants.TOKEN_COMMENT_CHAR) {
                                    if (blockCount) {
                                        blockCount--;
                                    } else {
                                        break;
                                    }
                                }
                            }
                            pos++;
                            char = code.charAt(pos);
                        }
                        if (pos == code.length) {
                            throwParserError({ msn: "Open comment block.", line: lineCount });
                        } else {
                            if (tokens.length == 0) {
                                codeName = code.substring(start + 2, pos - 1);
                            }
                        }
                    } else {//its a comment line
                        while (pos < code.length && char != '\n') {
                            pos++;
                            char = code.charAt(pos);
                        }
                    }
                } else {
                    isReading = true;
                    start = pos;
                }
            }
            if (char == '\n') {
                lineCount++;
            }
            pos++;
        }
        if (listCount > 0) {
            throw "Unfinished List definition\n";
        }
        if (commandStartList > 0) {
            throw "Unfinished Command definition\n";
        }
        return tokens;
    }
}

module.exports = SRLParser