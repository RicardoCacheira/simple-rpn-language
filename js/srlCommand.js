const tokenType = require("./tokenType");
module.exports = class SRLCommand {
    constructor(tokens) {
        this.tokens = tokens;
    }

    concat(com) {
        return new SRLCommand(this.tokens.concat(com.tokens));
    }

    toList() {
        var res = []
        for (var i = 0; i < this.tokens.length; i++) {
            var t = this.tokens[i];
            if (t.token == tokenType.LIST_START) {
                var level = 1;
                var start = i;
                do {
                    i++;
                    t = this.tokens[i];
                    if (t.token == tokenType.LIST_START) {
                        level++;
                    } else if (t.token == tokenType.LIST_END) {
                        level--;
                    }
                } while (level)
                res.push(new SRLCommand(this.tokens.slice(start, i + 1)));
            } else if (t.token == tokenType.BLOCK_START) {
                var level = 1;
                var start = i;
                do {
                    i++;
                    t = this.tokens[i];
                    if (t.token == tokenType.BLOCK_START) {
                        level++;
                    } else if (t.token == tokenType.BLOCK_END) {
                        level--;
                    }
                } while (level)
                res.push(new SRLCommand(this.tokens.slice(start, i + 1)));
            } else {
                res.push(new SRLCommand([t]));
            }
        }
        return res;
    }

    toTokenList() {
        var res = []
        for (var i = 0; i < this.tokens.length; i++) {
            var t = this.tokens[i];
            if (t.token == tokenType.LIST_START || t.token == tokenType.LIST_END || t.token == tokenType.BLOCK_START || t.token == tokenType.BLOCK_END) {
                continue
            } else if (t.token == tokenType.COMMAND) {
                res.push(...t.value.toTokenList());
            } else {
                res.push(new SRLCommand([t]));
            }
        }
        return res;
    }

    replace(str, com) {
        var res = []
        for (var t of this.tokens) {
            if (t.token == tokenType.VAR) {
                if (t.value == str) {
                    res.push(...com.tokens);
                } else {
                    res.push(t);
                }
            } else if (t.token == tokenType.COMMAND) {
                res.push(t.value.replace(str, com));
            } else {
                res.push(t);
            }
        }
        return new SRLCommand(res);
    }

    rename(oldName, newName) {
        var res = []
        for (var t of this.tokens) {
            if (t.token == tokenType.VAR || t.token == tokenType.TOP_VAR || t.token == tokenType.SET_VAR || t.token == tokenType.SET_TOP_VAR || t.token == tokenType.GET_VAR || t.token == tokenType.GET_TOP_VAR) {
                if (t.value == oldName) {
                    res.push({ token: t.token, value: newName, start: t.start, end: t.end, line: t.line, toString: t.toString });
                } else {
                    res.push(t);
                }
            } else if (t.token == tokenType.COMMAND) {
                res.push(t.value.rename(oldName, newName));
            } else {
                res.push(t);
            }
        }
        return new SRLCommand(res);
    }

    toString() {
        var str = "(";
        for (var t of this.tokens) {
            str += " " + t.toString();
        }
        return str + " )"
    }

    equals(o) {
        if (o instanceof SRLCommand && this.tokens.length == o.tokens.length) {
            for (var i = 0; i < this.tokens.length; i++) {
                if (this.tokens[i].token != o.tokens[i].token || this.tokens[i].value != o.tokens[i].value) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}