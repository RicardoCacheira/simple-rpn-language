module.exports = class SRLFunction {
    constructor(params, command) {
        this.params = params;
        this.command = command;
    }

    paramList() {
        return this.params
    }

    toCommand() {
        return this.command
    }

    replace(str, com) {
        return new SRLFunction(this.params, this.command.replace(str, com));
    }

    rename(oldName, newName) {
        return new SRLFunction(this.params, this.command.rename(oldName, newName));
    }

    toString() {
        var str = "";
        for (var p of this.params) {
            str += "<" + p.toString() + "> ";
        }
        return str + this.command.toString();
    }

    checkAndRun(sys) {
        if (sys.areValidParams(this.params)) {
            sys.processCommand(this.command);
        } else {
            sys.throwCurrentTokenError("Invalid function parameters");
        }
    }

    run(sys) {
        sys.processCommand(this.command)
    }

    equals(o) {
        if (o instanceof SRLFunction && this.params.length == o.params.length) {
            for (var i = 0; i < this.params.length; i++) {
                if (this.params[i] != o.params[i]) {
                    return false;
                }
            }
            return this.command.equals(o.command);
        }
        return false;
    }
}