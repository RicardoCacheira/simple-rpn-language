module.exports = class SRLMultiFunction {
    constructor(funcList) {
        if (funcList.length == 0) {
            throw "Empty function list";
        }
        this.funcList = funcList;
    }

    toList() {
        return this.funcList;
    }

    replace(str, com) {
        var res = [];
        for (var f of this.funcList) {
            res.push(f.replace(str, com));
        }
        return new SRLMultiFunction(res);
    }

    rename(oldName, newName) {
        var res = [];
        for (var f of this.funcList) {
            res.push(f.rename(oldName, newName));
        }
        return new SRLMultiFunction(res);
    }

    toString() {
        var res = "|";
        for (var f of this.funcList) {
            res += f.toString() + "|";
        }
        return res;
    }

    checkAndRun(sys) {
        for (var f of this.funcList) {
            if (sys.areValidParams(f.params)) {
                f.run(sys)
                return
            }
        }
        sys.throwCurrentTokenError("No valid parameters found for the function");
    }

    equals(o) {
        if (o instanceof SRLMultiFunction && this.funcList.length == o.funcList.length) {
            for (var i = 0; i < this.funcList.length; i++) {
                if (!this.params[i].equals(o.params[i])) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }
}