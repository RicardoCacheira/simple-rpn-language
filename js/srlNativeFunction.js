module.exports = class SRLNativeFunction {
    constructor(params, nativeFunc) {
        this.params = params
        this.nativeFunc = nativeFunc;
    }

    paramList() {
        return this.params
    }

    toString() {
        var str = "";
        for (var p of this.params) {
            str += "<" + p.toString() + "> ";
        }
        return str + "(native function)";
    }

    checkAndRun(sys) {
        if (sys.areValidParams(this.params)) {
            this.nativeFunc(sys, ...sys.getParams(this.params.length));
        } else {
            sys.throwCurrentTokenError("Invalid function parameters");
        }
    }

    run(sys) {
        this.nativeFunc(sys, ...sys.getParams(this.params.length));
    }

    equals(o) {
        if (o instanceof SRLFunction && this.params.length == o.params.length) {
            for (var i = 0; i < this.params.length; i++) {
                if (this.params[i] != o.params[i]) {
                    return false;
                }
            }
            return this.command.equals(o.command);
        }
        return false;
    }
}