class SRLType {
    constructor(name) {
        this.name = name;
    }
    toString() {
        return this.name;
    }
    equals(o) {
        return this === o;
    }
}

class NativeType extends SRLType {
    constructor(name) {
        super(name);
    }
}

class CustomType extends SRLType {
    constructor(name, checkerCom, toStringCom) {
        super(name);
        this.checkerCom = checkerCom;
        this.toStringCom = toStringCom;
    }

    createInstance(sys, value) {
        var x = this;
        sys.push(value);
        sys.processCommand(this.checkerCom,
            function () {
                if (sys.stackSize() > 0) {
                    var res = sys.pop();
                    if (typeof res == "boolean") {
                        if (res) {
                            sys.push(value);
                            sys.processCommand(x.toStringCom, function () {
                                if (sys.stackSize() > 0) {
                                    var res = sys.pop();
                                    if (typeof res == "string") {
                                        sys.push(new CustomTypeInstance(x, value, res));
                                    } else {
                                        sys.throwCurrentTokenError("Non string value on top of the stack after \"" + x.name + "\" toString command");
                                    }
                                } else {
                                    sys.throwCurrentTokenError("Empty Stack \"" + x.name + "\" toString command");
                                }
                            }, function () {
                                sys.throwCurrentTokenError("Error running the \"" + x.name + "\" toString command");
                            });
                        } else {
                            sys.throwCurrentTokenError("Invalid \"" + x.name + "\" type value");
                        }
                    } else {
                        sys.throwCurrentTokenError("Non boolean value on top of the stack after \"" + x.name + "\" type check");
                    }
                } else {
                    sys.throwCurrentTokenError("Empty Stack \"" + x.name + "\" type check");
                }
            }, function () {
                sys.throwCurrentTokenError("Error running the \"" + x.name + "\" type check");
            });
    }
}

class CustomTypeInstance {
    constructor(type, value, str) {
        this.type = type;
        this.value = value;
        this.str = str;
    }
    equals(o) {
        if (o instanceof CustomTypeInstance && this.type === o.type) {
            return utils.equals(this.value, o.value);
        }
        return false;
    }
    toString() {
        return this.str;
    }
}

module.exports.SRLType = SRLType;
module.exports.NativeType = NativeType;
module.exports.nativeTypeList = {
    NUMBER: new NativeType("Number"),
    STRING: new NativeType("String"),
    BOOLEAN: new NativeType("Boolean"),
    LIST: new NativeType("List"),
    COMMAND: new NativeType("Command"),
    TYPE: new NativeType("Type"),
    FUNCTION: new NativeType("Function"),
    NATIVE_FUNCTION: new NativeType("NativeFunction"),
    MULTI_FUNCTION: new NativeType("MultiFunction"),
    KEY: new NativeType("Key"),
    ANY: new NativeType("Any")
};
module.exports.CustomType = CustomType;
module.exports.CustomTypeInstance = CustomTypeInstance;