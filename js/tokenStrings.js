const constants = require("./constants");
const utils = require("./utils");

module.exports.VAR = function () {
    return this.value;
};
module.exports.TOP_VAR = function () {
    return constants.TOKEN_TOP_VAR + this.name;
};
module.exports.LOCAL_VAR = function () {
    return constants.TOKEN_LOCAL_VAR + this.value;
};
module.exports.GET_VAR = function () {
    return constants.TOKEN_GET_VAR + this.value;
};
module.exports.GET_TOP_VAR = function () {
    return constants.TOKEN_GET_VAR + constants.TOKEN_TOP_VAR + this.value;
};
module.exports.GET_LOCAL_VAR = function () {
    return constants.TOKEN_GET_VAR + constants.TOKEN_LOCAL_VAR + this.name;
};
module.exports.SET_VAR = function () {
    return constants.TOKEN_SET_VAR + this.value;
};
module.exports.SET_TOP_VAR = function () {
    return constants.TOKEN_SET_VAR + constants.TOKEN_TOP_VAR + this.value;
};
module.exports.SET_LOCAL_VAR = function () {
    return constants.TOKEN_SET_VAR + constants.TOKEN_LOCAL_VAR + this.name;
};
module.exports.NUMBER = function () {
    return "" + this.value;
};
module.exports.STRING = function () {
    return utils.formattedString(this.value);
};
module.exports.BOOLEAN = function () {
    if (this.value) {
        return constants.TOKEN_TRUE;
    }
    return constants.TOKEN_FALSE;
};
module.exports.LIST_START = function () {
    return constants.TOKEN_LIST_START;
};
module.exports.LIST_END = function () {
    return constants.TOKEN_LIST_END;
};
module.exports.BLOCK_START = function () {
    return constants.TOKEN_BLOCK_START;
};
module.exports.BLOCK_END = function () {
    return constants.TOKEN_BLOCK_END;
};
module.exports.COMMAND = function () {
    return this.value.toString();
};
module.exports.STRING_COMMAND = function () {
    var res = "\"";
    for (var part of this.value) {
        if (typeof part == "string") {
            res += utils.formattedStringNoToken(part);
        } else {
            res += "\\(" + part.join(" ") + ")";
        }
    }
    return res + "\"";
};
module.exports.EXIT = function () {
    return constants.TOKEN_EXIT;
};
module.exports.NONE = function () {
    return "";
};