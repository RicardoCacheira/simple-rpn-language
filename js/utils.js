const constants = require('./constants');

function repeatChar(char, num) {
    var res = "";
    for (var i = 0; i < num; i++) {
        res += char;
    }
    return res;
}

function equals(x, o) {
    if (typeof x == "object") {
        if (x instanceof Array) {
            return listEquals(x, o);
        } else {
            return x.equals(o);
        }
    } else {
        return x === o
    }
}

function contains(list, o) {
    for (var x of list) {
        if (equals(x, o)) {
            return true;
        }
    }
    return false;
}

function formattedString(str) {
    return constants.TOKEN_STRING_CHAR + formattedStringNoToken(str) + constants.TOKEN_STRING_CHAR
}

function formattedStringNoToken(str) {
    return str.replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t").replace(/"/g, '\\"').replace(/\u001b/g, '\\e');
}

function listEquals(list, o) {
    if (o instanceof Array && list.length == o.length) {
        for (var i = 0; i < list.length; i++) {
            if (!equals(list[i], o[i])) {
                return false;
            }
        }
        return true;
    }
    return false;
}

function stringReplace(src, token, rep) {
    if (token.length) {
        var res = "";
        var startChar = token.charAt(0);
        var isEqual;
        var pos;
        var aux;
        for (var i = 0; i < src.length; i++) {
            if (src.charAt(i) == startChar) {
                isEqual = true;
                pos = 1;
                while (isEqual && pos < token.length) {
                    aux = src.charAt(i + pos);
                    if (aux) {
                        if (aux != token.charAt(pos)) {
                            isEqual = false;
                        }
                    } else {
                        isEqual = false
                    }
                    pos++;
                }
                if (isEqual) {
                    i += pos - 1;
                    res += rep;
                } else if (aux) {
                    res += src.charAt(i);
                } else {
                    res += src.substring(i);
                    i = src.length;
                }
            } else {
                res += src.charAt(i);
            }
        }
        return res;
    }
    return src;
}

function copyList(list) {
    var res = []
    for (var x of list) {
        res.push(x);
    }
    return res;
}

function copyMap(list) {
    var res = new Map
    for (var x of list) {
        res.set(...x);
    }
    return res;
}

function generateRandomChar() {
    var x = Math.random() * 62 | 0;
    if (x < 10) {
        return String.fromCharCode(48 + x);//48=='0'
    }
    if (x < 36) {
        return String.fromCharCode(55 + x);//65=='A' 65-10=55
    }
    return String.fromCharCode(61 + x);//97=='a' 97-36=61
}

function generateRandomString(size) {
    var res = ""
    for (var i = 0; i < size; i++) {
        res += generateRandomChar();
    }
    return res
}

module.exports.repeatChar = repeatChar;
module.exports.equals = equals;
module.exports.contains = contains;
module.exports.formattedString = formattedString;
module.exports.formattedStringNoToken = formattedStringNoToken;
module.exports.stringReplace = stringReplace;
module.exports.copyList = copyList;
module.exports.copyMap = copyMap;
module.exports.generateRandomString = generateRandomString;