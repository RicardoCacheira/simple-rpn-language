/*
    Sprite server format
    [ width, spriteStr ]
    The spriteStr is an hexadecimal representation of the sprite
    The first char is the least significant half-byte
    e.g.

    .###.
    #...#
    #...#
    #...#
    .###.

    split this into 4 chars

    .###|.
    #..|.#
    #.|..#
    #|...#|
    .###|.

    .###
    .#..
    .##.
    ..##
    ...#
    .###
    .

    Convert into hexadecimal
    .### - 0111 - 7
    .#.. - 0100 - 4
    .##. - 0110 - 6
    ..## - 0011 - 3
    ...# - 0001 - 1
    .### - 0111 - 7
    .    - 0000 - 0

    sprite string: e26c8e0

    final value [ 5, "e26c8e0" ]
*/

const keyList = require("./inputKey").list;

function client2inputKey(key) {
    switch (key) {
        case "ArrowUp":
            return keyList.KEY_UP;
        case "ArrowDown":
            return keyList.KEY_DOWN;
        case "ArrowLeft":
            return keyList.KEY_LEFT;
        case "ArrowRight":
            return keyList.KEY_RIGHT;
        case "Enter":
            return keyList.KEY_RETURN;
        case "Backspace":
            return keyList.KEY_BS;
        case "Escape":
            return keyList.KEY_ESC;
        case "Tab":
            return keyList.KEY_TAB;
        case "Delete":
            return keyList.KEY_DEL;
        case "Home":
            return keyList.KEY_HOME;
        case "End":
            return keyList.KEY_END;
        case "PageUp":
            return keyList.KEY_PG_UP;
        case "PageDown":
            return keyList.KEY_PG_DOWN;
        case "F1":
            return keyList.KEY_F1;
        case "F2":
            return keyList.KEY_F2;
        case "F3":
            return keyList.KEY_F3;
        case "F4":
            return keyList.KEY_F4;
        case "F5":
            return keyList.KEY_F5;
        case "F6":
            return keyList.KEY_F6;
        case "F7":
            return keyList.KEY_F7;
        case "F8":
            return keyList.KEY_F8;
        case "F9":
            return keyList.KEY_F9;
        case "F10":
            return keyList.KEY_F10;
        case "F11":
            return keyList.KEY_F11;
        case "F12":
            return keyList.KEY_F12;
    }
    return key;
}

class WSGUISever {
    constructor() {
        this.width = 0;
        this.height = 0;
        this.sprites = [];
        this.color = [255, 255, 255];
        this.backgroundColor = [0, 0, 0];
        this.center = true;
        this.limit = [0, 0];
        this.keyQueue = [];
        this.mouseClickQueue = [];
        this.mouseMoveQueue = [];
    }

    _setDimensions(width, height) {
        this.width = width;
        this.height = height;
    }

    endConnection() {
        this._send("end");
    }

    setConnection(wsConnection) {
        this._send("end");
        this.wsConnection = wsConnection;
        var wsgui = this;
        wsConnection.on("text", function (msn) {
            msn = msn.split(";");
            switch (msn.shift()) {
                case "dimensions":
                    wsgui._setDimensions(parseInt(msn[0]), parseInt(msn[1]));
                    break;
                case "input":
                    if (wsgui.keyQueue.length) {
                        wsgui.keyQueue.shift()(client2inputKey(msn[0]));
                    }
                    break;
                case "mousec":
                    if (wsgui.mouseClickQueue.length) {
                        wsgui.mouseClickQueue.shift()(msn.map((val) => { return Number.parseInt(val) }));
                    }
                    break;
                case "mousem":
                    if (wsgui.mouseMoveQueue.length) {
                        wsgui.mouseMoveQueue.shift()(msn.map((val) => { return Number.parseInt(val) }));
                    }
                    break;
            }
        })
        wsConnection.on("close", function () {
            if (wsgui.wsConnection === wsConnection) {
                wsgui.wsConnection = undefined
                wsgui.width = 0;
                wsgui.height = 0;
            }
        })
        //Syncronises the server info with the client
        for (var sprite of this.sprites) {
            this._sendSprite(sprite);
        }
        this._send("color", ...this.color);
        this._send("bcolor", ...this.backgroundColor);
        this._send("limit", ...this.limit);
        this._send("center", this.center ? "1" : "0");
    }

    _send(...args) {
        if (this.wsConnection) {
            this.wsConnection.sendText(args.join(";"));
        }
    }

    _sendSprite(sprite) {
        this._send("lsprite", ...sprite);
    }

    loadSprite(sprite) {
        var pos = this.sprites.length;
        this.sprites.push(sprite);
        this._sendSprite(sprite);
        return pos;
    }

    dropSprites() {
        this.sprites = [];
        this._send("dsprite");
    }

    setColor(r, g, b) {
        this.color = [r, g, b];
        this._send("color", r, g, b);
    }

    setBackgroundColor(r, g, b) {
        this.backgroundColor = [r, g, b];
        this._send("bcolor", r, g, b);
    }

    drawPoint(x, y) {
        this._send("point", x, y);
    }

    drawFRect(x, y, width, height) {
        this._send("frect", x, y, width, height);
    }

    drawSprite(x, y, spriteId) {
        if (spriteId < this.sprites.length) {
            this._send("sprite", x, y, spriteId);
        }
    }

    drawScaledSprite(x, y, spriteId, scale) {
        if (spriteId < this.sprites.length) {
            this._send("ssprite", x, y, spriteId, scale);
        }
    }

    setLimit(x, y) {
        this.limit = [x, y];
        this._send("limit", x, y);
    }

    setCenter(center) {
        this.center = center;
        this._send("center", center ? "1" : "0");
    }

    render() {
        this._send("render");
    }

    clear() {
        this._send("clear");
    }

    getDimensions() {
        return [this.width, this.height];
    }

    getPriorityKey(callback) {
        this.keyQueue.unshift(callback);
    }

    getKey(callback) {
        this.keyQueue.push(callback);
    }

    getMouseClick(callback) {
        this.mouseClickQueue.push(callback);
    }

    getMouseMove(callback) {
        this.mouseMoveQueue.push(callback);
    }

    removeGetKey(callback) {
        var pos = this.keyQueue.indexOf(callback);
        if (pos != -1) {
            this.keyQueue.splice(pos, 1);
        }
    }
}

module.exports = WSGUISever;