#!/usr/bin/env node
const nativeCommands = require("./js/nativeCommands");
const consoleSystem = require("./js/consoleSystem");
const WSGUISever = require("./js/wsguiSever");
const createInterpreter = require("./js/interpreter").createInterpreter;
const srlBootstrap = "/.srl_bootstrap";
const guiSystem = new WSGUISever();

process.on('exit', function () {
    guiSystem.endConnection();
    process.stdout.write(String.fromCharCode(27) + "[?25h" + String.fromCharCode(27) + "[0m\n");
});

process.on('SIGINT', function () {
    process.exit();
});

const exitFunc = function () {
    guiSystem.endConnection();
    process.exit();
};

process.stdout.write(String.fromCharCode(27) + "[?25l");

var interpreter = createInterpreter(nativeCommands, consoleSystem, guiSystem, exitFunc);

/*
SYSTEM - path where the interpreter is
ARGS - Received arguments
*/
var dir = interpreter.fileSystem.resolve(__dirname)

interpreter.vars[0].set("ARGS", process.argv.splice(2, process.argv.length - 2));
interpreter.vars[0].set("OS", process.platform);
interpreter.vars[0].set("SYSTEM", dir)

try {
    interpreter.loadFile(dir + srlBootstrap);
    interpreter.run();
} catch (e) {
    consoleSystem.error('No bootstrap file found.')
    exitFunc()
}