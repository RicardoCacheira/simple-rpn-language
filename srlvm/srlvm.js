const opcode = {
    "NUM": 0x01,
    "STR": 0x02,
    "TRUE": 0x03,
    "FALSE": 0x04,
    "ADD": 0x05,
    "SUB": 0x06,
    "MUL": 0x07,
    "DIV": 0x08,
    "MOD": 0x09,
    "DOUBLE": 0x10,
    "FLIP": 0x11,
    "POP": 0x12
}

const varType = {
    "NUM": 0x00,
    "STR": 0x01,
    "LIST": 0x02,
    "TRUE": 0x03,
    "FALSE": 0x03
}

function opName(op) {
    for (var name in opcode) {
        if (opcode[name] == op) {
            return name;
        }
    }
}

function assembler(code) {
    code = code.split("\n");
    var res = [];
    var resSize = 0;
    for (var line of code) {
        if (line.length) {
            var end = line.indexOf(" ");
            if (end == -1) {
                end = line.length;
            }
            var op = line.substring(0, end);

            switch (op) {
                case "NUM":
                    resSize += 9;
                    res.push({ op: opcode.NUM, val: Number.parseFloat(line.substring(end + 1)) });
                    break;
                case "STR":
                    resSize += 1;
                    res.push({ op: opcode.STR, val: "" });
                    break;
                case "TRUE":
                    resSize += 1;
                    res.push({ op: opcode.TRUE });
                    break;
                case "FALSE":
                    resSize += 1;
                    res.push({ op: opcode.FALSE });
                    break;
                case "ADD":
                    resSize += 1;
                    res.push({ op: opcode.ADD });
                    break;
                case "SUB":
                    resSize += 1;
                    res.push({ op: opcode.SUB });
                    break;
                case "MUL":
                    resSize += 1;
                    res.push({ op: opcode.MUL });
                    break;
                case "DIV":
                    resSize += 1;
                    res.push({ op: opcode.DIV });
                    break;
                case "MOD":
                    resSize += 1;
                    res.push({ op: opcode.MOD });
                    break;
                case "DOUBLE":
                    resSize += 1;
                    res.push({ op: opcode.DOUBLE });
                    break;
                case "FLIP":
                    resSize += 1;
                    res.push({ op: opcode.FLIP });
                    break;
                case "POP":
                    resSize += 1;
                    res.push({ op: opcode.POP });
                    break;
                default:
                    throw "Invalid opcode '" + op + "'";
            }
        }
    }
    console.log(res);
    var final = Buffer.allocUnsafe(resSize);
    var finalPos = 0;
    for (var line of res) {
        switch (line.op) {
            case opcode.NUM:
                final.writeUInt8(line.op, finalPos);
                finalPos++;
                final.writeDoubleLE(line.val, finalPos);
                finalPos += 8;
                break;
            case opcode.STR:
                final.writeUInt8(line.op, finalPos);
                finalPos++;
                break;
            default:
                final.writeUInt8(line.op, finalPos);
                finalPos++;
                break;
        }
    }
    return final
}

class VM {
    constructor() {
        this.stack = Buffer.allocUnsafe(65536);//64KB;
        this.auxBuffer = Buffer.allocUnsafe(1);
        this.stackPointer = 0;
        this.stackSize = 0;
        this.program = undefined;
        this.programPointer = 0;
    }
    loadProgramStr(str) {
        this.loadProgram(assembler(str));
    }
    loadProgram(buffer) {
        this.program = buffer;
        this.programSize = buffer.length;
        this.programPointer = 0;
    }
    pop() {
        if (this.stackSize > 0) {
            this.stackPointer -= 9;
            this.stackSize--;
            var type = this.stack.readUInt8(this.stackPointer)
            var res;
            switch (type) {
                case varType.NUM:
                    res = this.stack.readDoubleLE(this.stackPointer + 1);
                    break;
                case varType.STR:
                    break;
                case varType.LIST:
                    break;
                case varType.TRUE:
                    res = true;
                case varType.FALSE:
                    res = false;
            }
            return res;
        } else {
            throw "Popping an Empty Stack"
        }
    }

    double() {
        if (this.stackSize > 0) {
            this.stack[this.stackPointer] = this.stack[this.stackPointer - 9];
            this.stack[this.stackPointer + 1] = this.stack[this.stackPointer - 8];
            this.stack[this.stackPointer + 2] = this.stack[this.stackPointer - 7];
            this.stack[this.stackPointer + 3] = this.stack[this.stackPointer - 6];
            this.stack[this.stackPointer + 4] = this.stack[this.stackPointer - 5];
            this.stack[this.stackPointer + 5] = this.stack[this.stackPointer - 4];
            this.stack[this.stackPointer + 6] = this.stack[this.stackPointer - 3];
            this.stack[this.stackPointer + 7] = this.stack[this.stackPointer - 2];
            this.stack[this.stackPointer + 8] = this.stack[this.stackPointer - 1];
            this.stackPointer += 9;
            this.stackSize++;
        } else {
            throw "Doubling an Empty Stack"
        }
    }

    flip() {
        if (this.stackSize > 1) {
            this.auxBuffer[0] = this.stack[this.stackPointer - 1];
            this.stack[this.stackPointer - 1] = this.stack[this.stackPointer - 10];
            this.stack[this.stackPointer - 10] = this.auxBuffer[0];
            this.auxBuffer[0] = this.stack[this.stackPointer - 2];
            this.stack[this.stackPointer - 2] = this.stack[this.stackPointer - 11];
            this.stack[this.stackPointer - 11] = this.auxBuffer[0];
            this.auxBuffer[0] = this.stack[this.stackPointer - 3];
            this.stack[this.stackPointer - 3] = this.stack[this.stackPointer - 12];
            this.stack[this.stackPointer - 12] = this.auxBuffer[0];
            this.auxBuffer[0] = this.stack[this.stackPointer - 4];
            this.stack[this.stackPointer - 4] = this.stack[this.stackPointer - 13];
            this.stack[this.stackPointer - 13] = this.auxBuffer[0];
            this.auxBuffer[0] = this.stack[this.stackPointer - 5];
            this.stack[this.stackPointer - 5] = this.stack[this.stackPointer - 14];
            this.stack[this.stackPointer - 14] = this.auxBuffer[0];
            this.auxBuffer[0] = this.stack[this.stackPointer - 6];
            this.stack[this.stackPointer - 6] = this.stack[this.stackPointer - 15];
            this.stack[this.stackPointer - 15] = this.auxBuffer[0];
            this.auxBuffer[0] = this.stack[this.stackPointer - 7];
            this.stack[this.stackPointer - 7] = this.stack[this.stackPointer - 16];
            this.stack[this.stackPointer - 16] = this.auxBuffer[0];
            this.auxBuffer[0] = this.stack[this.stackPointer - 8];
            this.stack[this.stackPointer - 8] = this.stack[this.stackPointer - 17];
            this.stack[this.stackPointer - 17] = this.auxBuffer[0];
            this.auxBuffer[0] = this.stack[this.stackPointer - 9];
            this.stack[this.stackPointer - 9] = this.stack[this.stackPointer - 18];
            this.stack[this.stackPointer - 18] = this.auxBuffer[0];
        } else {
            throw "Fliping a Stack with less than 2 values"
        }
    }

    pushNum(num) {
        this.stack.writeUInt8(varType.NUM, this.stackPointer);
        this.stackPointer++;
        this.stack.writeDoubleLE(num, this.stackPointer);
        this.stackPointer += 8;
        this.stackSize++;
    }

    printStack() {
        var i = 0;
        var res = "[ ";
        var pointer = 0;
        for (var i = 0; i < this.stackSize; i++) {
            var type = this.stack.readUInt8(pointer)
            switch (type) {
                case varType.NUM:
                    res += this.stack.readDoubleLE(pointer + 1) + " ";
                    pointer += 9
                    break;
                case varType.STR:
                    break;
                case varType.LIST:
                    break;
                case varType.TRUE:
                    res = true;
                case varType.FALSE:
                    res = false;
            }
        }
        return res + "]";

    }

    run() {
        while (this.programPointer < this.programSize) {
            var op = this.program.readUInt8(this.programPointer)
            console.log("Prog Pointer: " + this.programPointer);
            console.log("Current OP: " + opName(op));
            this.programPointer++;
            switch (op) {
                case opcode.NUM:
                    this.pushNum(this.program.readDoubleLE(this.programPointer));
                    this.programPointer += 8;
                    break;
                case opcode.STR:
                    break;
                case opcode.TRUE:
                    break;
                case opcode.FALSE:
                    break;
                case opcode.ADD:
                    this.pushNum(this.pop() + this.pop());
                    break;
                case opcode.SUB:
                    var aux = this.pop();
                    this.pushNum(this.pop() - aux);
                    break;
                case opcode.MUL:
                    this.pushNum(this.pop() * this.pop());
                    break;
                case opcode.DIV:
                    var aux = this.pop();
                    this.pushNum(this.pop() / aux);
                    break;
                case opcode.MOD:
                    var aux = this.pop();
                    this.pushNum(this.pop() % aux);
                    break;
                case opcode.DOUBLE:
                    this.double();
                    break;
                case opcode.FLIP:
                    this.flip();
                    break;
                case opcode.POP:
                    this.pop();
                    break;
            }
            console.log("Stack Size: " + this.stackSize);
            console.log("Stack Pointer: " + this.stackPointer);
            console.log("Stack: " + this.printStack());
            console.log("");
        }
    }
}

var asm = `
NUM 5
NUM 2
ADD
NUM 5
NUM 2
SUB
NUM 5
NUM 2
MUL
NUM 5
NUM 2
DIV
DOUBLE
NUM 5
NUM 2
MOD
FLIP
POP
`

var vm = new VM();
vm.loadProgramStr(asm)
console.log(vm.program);
vm.run()