if (process.platform == "win32") {
    const fs = require("fs");
    const path = require("path");
    const exec = require("child_process").execSync
    console.log("Running npm unlink")
    exec("npm unlink")
    console.log("Reading package.json")
    var json = JSON.parse(fs.readFileSync("package.json"));
    console.log("Removing the generated .cmd files")
    for (var cmd in json.bin) {
        var binPath = path.resolve(json.bin[cmd])
        json.bin[cmd] = path.join(path.dirname(binPath), path.basename(binPath).split(".")[0] + ".cmd");
        fs.unlinkSync(json.bin[cmd])
    }
} else {
    const exec = require("child_process").execSync
    console.log("Running npm unlink")
    exec("npm unlink")
}